package cn.edu.njau.lufuser;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.MouseInfo;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.io.IOException;

import javax.swing.JPanel;

import jxl.read.biff.BiffException;

import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.awt.SWT_AWT;

import cn.edu.njau.lufuse.Lufuse;
import cn.edu.njau.lufuse.algo.MatrixChanger;
import cn.edu.njau.lufuse.io.MatrixXlsReader;
import cn.edu.njau.lufuse.util.SnapShot;
import cn.edu.njau.lufuse.vis.NewFrame;
import cn.edu.njau.lufuse.vis.SnsCluShow;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.custom.CTabItem;

import prefuse.data.Edge;
import prefuse.data.Graph;
import prefuse.data.Node;
import prefuse.util.FontLib;

import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

public class Lufuser {

	public static String PRO_NAME = "Lufuser"; // 项目名称
	public static String PRO_VERSION = "0.1.4.108"; // 版本号
	public static String PRO_AUTHOR = "lujian863"; // 作者
	public static String AUT_MAIL = "lujian863@qq.com";
	public static String PRO_DATE = "2013-01-08"; // 更新日期

	public static String XLS_MAR_PATH = null;
	public static String XML_GRA_PATH = null;
	public static int FILE_KIND = 0;

	protected Shell shell;
	private Composite composite;
	private Frame myframe;
	private JPanel panel;
	private StyledText sss;
	private Graph gra;
	private Button btnStart;
	private Button btnBigRun;
	private Button btnPrePro;
	private Tree nodeTree;
	private Tree edgeTree;
	private CTabFolder tabFolder;

	private SnsCluShow scs = null;
	private NewFrame nf = null;

	public static void main(String[] args) {
		// 从配置文件加载数据

		// 启动窗口
		try {
			Lufuser window = new Lufuser();
			// Lufuser2 window = new Lufuser2();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		org.eclipse.swt.widgets.Display display = org.eclipse.swt.widgets.Display
				.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell(SWT.MIN);
		shell.setMinimumSize(new Point(1000, 700));
		shell.setSize(1080, 700);
		shell.setText(Lufuser.PRO_NAME + "-v" + Lufuser.PRO_VERSION + " by "
				+ Lufuser.PRO_AUTHOR);

		// 设置窗体居中显示
		int scrWid = shell.getMonitor().getClientArea().width;
		int scrHei = shell.getMonitor().getClientArea().height;
		int x = shell.getSize().x;
		int y = shell.getSize().y;
		if (x > scrWid) {
			shell.getSize().x = scrWid;
		}
		if (y > scrHei) {
			shell.getSize().y = scrHei;
		}
		shell.setLocation((scrWid - x) / 2, (scrHei - y) / 2);

		Menu menu = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menu);

		MenuItem menuItem = new MenuItem(menu, SWT.CASCADE);
		menuItem.setText("\u6587\u4EF6");

		Menu menu_3 = new Menu(menuItem);
		menuItem.setMenu(menu_3);

		MenuItem mntmxls = new MenuItem(menu_3, SWT.NONE);
		mntmxls.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 打开 xls 文件
				FileDialog fs = new FileDialog(shell, SWT.SINGLE);
				fs.setFilterNames(new String[] { "*.xls" });
				fs.setFilterExtensions(new String[] { "*.xls" });
				String fp = fs.open();
				if (fp == null) {
					sss.append("未选择文件！\n");
					sss.setSelection(sss.getCharCount());
				} else {
					btnPrePro.setEnabled(true);
					FILE_KIND = 1;
					XLS_MAR_PATH = fp;
					sss.append("已选择文件：" + XLS_MAR_PATH + "！\n");
					sss.setSelection(sss.getCharCount());
				}
			}
		});
		mntmxls.setText("\u5BFC\u5165Excel\u6587\u4EF6");

		MenuItem mntmxml = new MenuItem(menu_3, SWT.NONE);
		mntmxml.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				// 打开XML文件
				FileDialog fs = new FileDialog(shell, SWT.SINGLE);
				fs.setFilterNames(new String[] { "*.xml" });
				fs.setFilterExtensions(new String[] { "*.xml" });
				String fp = fs.open();
				if (fp == null) {
					sss.append("未选择文件！\n");
					sss.setSelection(sss.getCharCount());
				} else {
					FILE_KIND = 2;
					XML_GRA_PATH = fp;
					sss.append("已选择文件：" + XML_GRA_PATH + "！\n");
					sss.setSelection(sss.getCharCount());
				}

			}
		});
		mntmxml.setText("\u5BFC\u5165XML\u6587\u4EF6");

		MenuItem menuItem_3 = new MenuItem(menu, SWT.NONE);
		menuItem_3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 截图
				SnapShot.getImg();
			}
		});
		menuItem_3.setText("\u622A\u56FE");

		MenuItem mntmNode = new MenuItem(menu, SWT.CASCADE);
		mntmNode.setText("Node\u663E\u793A");

		Menu menu_2 = new Menu(mntmNode);
		mntmNode.setMenu(menu_2);

		MenuItem mntmNewItem = new MenuItem(menu_2, SWT.NONE);
		mntmNewItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 显示编号
				Lufuse.NODE_NAME = "node_id";
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmNewItem.setText("\u663E\u793A\u7F16\u53F7");

		MenuItem mntmNewItem_1 = new MenuItem(menu_2, SWT.NONE);
		mntmNewItem_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 显示标签
				Lufuse.NODE_NAME = "node_tag";
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmNewItem_1.setText("\u663E\u793A\u540D\u79F0");

		MenuItem mntmNode_1 = new MenuItem(menu, SWT.CASCADE);
		mntmNode_1.setText("Node\u957F\u5EA6");

		Menu menu_4 = new Menu(mntmNode_1);
		mntmNode_1.setMenu(menu_4);

		MenuItem mntmMax = new MenuItem(menu_4, SWT.NONE);
		mntmMax.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.NODE_LENGTH = 50;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmMax.setText("max = 50");

		MenuItem mntmMax_1 = new MenuItem(menu_4, SWT.NONE);
		mntmMax_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.NODE_LENGTH = 80;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmMax_1.setText("max = 80 (\u9ED8\u8BA4\u503C)");

		MenuItem mntmMax_2 = new MenuItem(menu_4, SWT.NONE);
		mntmMax_2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.NODE_LENGTH = 120;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmMax_2.setText("max = 120");

		MenuItem mntmMax_6 = new MenuItem(menu_4, SWT.NONE);
		mntmMax_6.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.NODE_LENGTH = 200;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmMax_6.setText("max = 200");

		MenuItem mntmMax_7 = new MenuItem(menu_4, SWT.NONE);
		mntmMax_7.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.NODE_LENGTH = 500;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmMax_7.setText("max = 500");

		MenuItem mntmNewSubmenu = new MenuItem(menu, SWT.CASCADE);
		mntmNewSubmenu.setText("Edge\u957F\u5EA6");

		Menu menu_1 = new Menu(mntmNewSubmenu);
		mntmNewSubmenu.setMenu(menu_1);

		MenuItem mntmx_1 = new MenuItem(menu_1, SWT.NONE);
		mntmx_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 0.25x
				Lufuse.EDGE_RATE = 0.25f;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmx_1.setText("0.25x");

		MenuItem mntmNewItem_4 = new MenuItem(menu_1, SWT.NONE);
		mntmNewItem_4.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 1/2x
				Lufuse.EDGE_RATE = 0.5f;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmNewItem_4.setText("0.5x");

		MenuItem mntmNewItem_2 = new MenuItem(menu_1, SWT.NONE);
		mntmNewItem_2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 1x
				Lufuse.EDGE_RATE = 1.0f;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmNewItem_2.setText("1x");

		MenuItem mntmNewItem_5 = new MenuItem(menu_1, SWT.NONE);
		mntmNewItem_5.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 3/2x
				Lufuse.EDGE_RATE = 1.5f;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmNewItem_5.setText("1.5x");

		MenuItem mntmNewItem_3 = new MenuItem(menu_1, SWT.NONE);
		mntmNewItem_3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 2x
				Lufuse.EDGE_RATE = 2.0f;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmNewItem_3.setText("2x");

		MenuItem mntmx = new MenuItem(menu_1, SWT.NONE);
		mntmx.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 2.5x
				Lufuse.EDGE_RATE = 2.5f;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmx.setText("2.5x");

		MenuItem mntmx_2 = new MenuItem(menu_1, SWT.NONE);
		mntmx_2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 3x
				Lufuse.EDGE_RATE = 3.0f;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmx_2.setText("3x");

		MenuItem mntmEdge = new MenuItem(menu, SWT.CASCADE);
		mntmEdge.setText("Edge\u7C97\u7EC6");

		Menu menu_6 = new Menu(mntmEdge);
		mntmEdge.setMenu(menu_6);

		MenuItem mntmpx_10 = new MenuItem(menu_6, SWT.NONE);
		mntmpx_10.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.EDGE_BOUND = 0.0;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmpx_10.setText("0.0px");

		MenuItem mntmpx_5 = new MenuItem(menu_6, SWT.NONE);
		mntmpx_5.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.EDGE_BOUND = 0.25;
				try {
					refreshVis(gra);
					// 尝试使用刷新 display
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmpx_5.setText("0.25px");

		MenuItem mntmpx_6 = new MenuItem(menu_6, SWT.NONE);
		mntmpx_6.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.EDGE_BOUND = 0.5;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmpx_6.setText("0.5px");

		MenuItem mntmpx_11 = new MenuItem(menu_6, SWT.NONE);
		mntmpx_11.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.EDGE_BOUND = 0.75;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmpx_11.setText("0.75px");

		MenuItem mntmpx_7 = new MenuItem(menu_6, SWT.NONE);
		mntmpx_7.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.EDGE_BOUND = 1.0;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmpx_7.setText("1px (\u9ED8\u8BA4\u503C)");

		MenuItem mntmpx_8 = new MenuItem(menu_6, SWT.NONE);
		mntmpx_8.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.EDGE_BOUND = 1.5;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmpx_8.setText("1.5px");

		MenuItem mntmpx_9 = new MenuItem(menu_6, SWT.NONE);
		mntmpx_9.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.EDGE_BOUND = 2.0;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmpx_9.setText("2px");

		MenuItem mntmNode_2 = new MenuItem(menu, SWT.CASCADE);
		mntmNode_2.setText("\u5B57\u4F53\u5927\u5C0F");

		Menu menu_5 = new Menu(mntmNode_2);
		mntmNode_2.setMenu(menu_5);

		MenuItem mntmpx = new MenuItem(menu_5, SWT.NONE);
		mntmpx.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Node 增大
				Lufuse.NODE_FONT += 3;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmpx.setText("\u589E\u52A0 3px");

		MenuItem mntmpx_1 = new MenuItem(menu_5, SWT.NONE);
		mntmpx_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Node 减小
				Lufuse.NODE_FONT -= 3;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmpx_1.setText("\u51CF\u5C0F 3px");

		MenuItem mntmNewItem_8 = new MenuItem(menu_5, SWT.NONE);
		mntmNewItem_8.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 8px
				Lufuse.NODE_FONT = 8;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmNewItem_8.setText("8px");

		MenuItem mntmNewItem_9 = new MenuItem(menu_5, SWT.NONE);
		mntmNewItem_9.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.NODE_FONT = 12;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmNewItem_9.setText("12px");

		MenuItem mntmpx_2 = new MenuItem(menu_5, SWT.NONE);
		mntmpx_2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.NODE_FONT = 18;
				// refreshVis(gra);
				scs.setFont(FontLib.getFont("Tahoma", Lufuse.NODE_FONT));
			}
		});
		mntmpx_2.setText("18px");

		MenuItem mntmpx_3 = new MenuItem(menu_5, SWT.NONE);
		mntmpx_3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.NODE_FONT = 24;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmpx_3.setText("24px");

		MenuItem mntmpx_4 = new MenuItem(menu_5, SWT.NONE);
		mntmpx_4.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.NODE_FONT = 36;
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		mntmpx_4.setText("36px");

		MenuItem menuItem_4 = new MenuItem(menu, SWT.NONE);
		menuItem_4.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 最大化运行，打开一个新窗口
				nf = null;
				if (scs != null) {
					nf = new NewFrame(scs);
					nf.show();
				}
			}
		});
		menuItem_4.setText("\u5168\u5C4F\u663E\u793A");

		MenuItem mntmNewItem_6 = new MenuItem(menu, SWT.NONE);
		mntmNewItem_6.setText("\u5E2E\u52A9");

		MenuItem mntmNewItem_7 = new MenuItem(menu, SWT.NONE);
		mntmNewItem_7.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 关于
				MessageBox messageBox = new MessageBox(shell,
						SWT.ICON_INFORMATION | SWT.OK);
				messageBox.setMessage(Lufuser.PRO_NAME + " v"
						+ Lufuser.PRO_VERSION + "\nDev by "
						+ Lufuser.PRO_AUTHOR + "\nE-mail : " + Lufuser.AUT_MAIL
						+ " \nDate : " + Lufuser.PRO_DATE);
				messageBox.open();
			}
		});
		mntmNewItem_7.setText("\u5173\u4E8E");

		Group group = new Group(shell, SWT.NONE);
		group.setBounds(791, 0, 273, 514);
		group.setText("\u63A7\u5236\u83DC\u5355");

		btnPrePro = new Button(group, SWT.NONE);
		btnPrePro.setEnabled(false);
		btnPrePro.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 数据预处理
				Lufuse.VIS_WIDTH = panel.getSize().width;
				Lufuse.VIS_HEIGHT = panel.getSize().height;

				// 读取xls文件数据
				String[][] mat = null;
				try {
					mat = MatrixXlsReader.getMatrixData(XLS_MAR_PATH);
				} catch (BiffException e1) {
					e1.printStackTrace();
					sss.append("Error:12134");
				} catch (IOException e1) {
					e1.printStackTrace();
					sss.append("Error:12234");
				}

				//
				MatrixChanger mc = new MatrixChanger(mat);
				gra = mc.matrixToGraph();

				sss.append("数据预处理完成！\n");
				sss.setSelection(sss.getCharCount());
				btnPrePro.setEnabled(false);
			}
		});
		btnPrePro.setBounds(10, 25, 57, 27);
		btnPrePro.setText("\u9884\u5904\u7406");

		btnStart = new Button(group, SWT.NONE);
		btnStart.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				// 可视化
				try {
					switch (FILE_KIND) {
					// 没有文件
					case 0:
						sss.append("请选择导入的文件类型！\n");
						sss.setSelection(sss.getCharCount());
						break;
					// 使用 Excel 文件，导入 Graph
					case 1:
						sss.append("正在处理。。。\n");
						sss.setSelection(sss.getCharCount());

						refreshTree();

						scs = new SnsCluShow(gra);
						scs.setSize(panel.getSize());
						panel.add(scs);
						sss.append("绘图完成！\n");
						sss.setSelection(sss.getCharCount());
						refreshTree();
						FILE_KIND = 4;
						break;
					// 使用 Lufuser 默认 XML 文件
					case 2:
						sss.append("正在处理。。。");
						sss.setSelection(sss.getCharCount());

						scs = new SnsCluShow(XML_GRA_PATH);
						scs.setSize(panel.getSize());
						panel.add(scs);

						sss.append("绘图完成！\n");
						sss.setSelection(sss.getCharCount());

						refreshTree();
						FILE_KIND = 4;
						break;
					// 使用 Lufuse 默认 XML 文件
					case 3:
						sss.append("正在处理。。。\n");
						sss.setSelection(sss.getCharCount());

						scs = new SnsCluShow(Lufuse.GRA_XML_PATH);
						scs.setSize(panel.getSize());
						panel.add(scs);
						sss.append("绘图完成！\n");
						sss.setSelection(sss.getCharCount());

						refreshTree();
						FILE_KIND = 4;
						break;
					// 重绘
					case 4:
						sss.append("开始重绘。。。\n");
						sss.setSelection(sss.getCharCount());

						scs = new SnsCluShow(gra);
						scs.setSize(panel.getSize());
						panel.add(scs);

						sss.append("绘图完成！\n");
						sss.setSelection(sss.getCharCount());

						refreshTree();
						break;
					}
				} catch (IOException e1) {
					e1.printStackTrace();
					System.err.println("lujian863-init-scs");
				}
			}
		});
		btnStart.setBounds(137, 25, 63, 27);
		btnStart.setText("\u7ED8\u56FE");

		btnBigRun = new Button(group, SWT.NONE);
		btnBigRun.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// FitBtn 即 FitButton 作用是调整当前视图到最佳状态
				// 模拟鼠标右击， 找到 panel 的中心点，右击
				try {
					java.awt.Point mousepoint = MouseInfo.getPointerInfo()
							.getLocation();
					Dimension screenSize = Toolkit.getDefaultToolkit()
							.getScreenSize();
					Robot robot = new Robot();
					// 移动鼠标
					robot.mouseMove(screenSize.width / 2, screenSize.height / 2);// 大概估计Vis的位置
					robot.delay(10);
					robot.mousePress(InputEvent.BUTTON3_MASK); // 右击
					robot.delay(10);// 松开
					robot.mouseRelease(InputEvent.BUTTON3_MASK);
					robot.delay(10);
					robot.mouseMove(mousepoint.x, mousepoint.y); // 将鼠标移回原位
					sss.append("调整结束。");
				} catch (AWTException e11) {
					e11.printStackTrace();
				}

			}
		});
		btnBigRun.setText("FitBtn");
		btnBigRun.setBounds(73, 25, 57, 27);

		Button btnStop = new Button(group, SWT.NONE);
		btnStop.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				panel.removeAll();

				FILE_KIND = 4;
			}
		});
		btnStop.setText("\u505C\u6B62");
		btnStop.setBounds(206, 25, 57, 27);

		tabFolder = new CTabFolder(group, SWT.BORDER);
		tabFolder.setBounds(10, 58, 253, 446);
		tabFolder.setSimple(false);
		tabFolder.setSelectionBackground(Display.getCurrent().getSystemColor(
				SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));

		CTabItem tabClu = new CTabItem(tabFolder, SWT.NONE);
		tabClu.setText("Aggregate");

		Tree cluTree = new Tree(tabFolder, SWT.BORDER);
		tabClu.setControl(cluTree);

		TreeItem allNodes = new TreeItem(cluTree, SWT.NONE);
		allNodes.setText("New TreeItem");

		CTabItem tabColor = new CTabItem(tabFolder, SWT.NONE);
		tabColor.setText("Color");

		CTabItem tabNode = new CTabItem(tabFolder, SWT.NONE);
		tabNode.setText("Nodes");
		tabFolder.setSelection(tabNode);
		nodeTree = new Tree(tabFolder, SWT.BORDER | SWT.CHECK);
		tabNode.setControl(nodeTree);
		nodeTree.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent se) {
				// Nothing Serious
			}

			@Override
			public void widgetSelected(SelectionEvent se) {
				// 获取一个 Node，然后删除之~
				TreeItem ti = (TreeItem) se.item;
				Node node = (Node) ti.getData("node");
				gra.removeNode(node);
				try {
					refreshVis(gra);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				refreshTree();
			}
		});

		CTabItem tabEdge = new CTabItem(tabFolder, SWT.NONE);
		tabEdge.setText("Edges");
		edgeTree = new Tree(tabFolder, SWT.BORDER | SWT.CHECK);
		tabEdge.setControl(edgeTree);

		Group group_1 = new Group(shell, SWT.NONE);
		group_1.setText("\u53EF\u89C6\u5316");
		group_1.setBounds(10, 0, 775, 642);

		composite = new Composite(group_1, SWT.EMBEDDED);
		composite.setBounds(10, 20, 755, 612);

		myframe = SWT_AWT.new_Frame(composite);
		myframe.setSize(composite.getSize().x, composite.getSize().y);
		panel = new JPanel();
		panel.setSize(myframe.getSize());

		myframe.add(panel);

		Group group_2 = new Group(shell, SWT.NONE);
		group_2.setText("\u7CFB\u7EDF\u72B6\u6001");
		group_2.setBounds(791, 520, 273, 122);

		sss = new StyledText(group_2, SWT.BORDER | SWT.READ_ONLY | SWT.V_SCROLL);
		sss.setTouchEnabled(true);
		sss.setBounds(10, 23, 253, 89);
	}

	// 重新加载所有 TreeItem
	private void refreshTree() {
		// 获取所有 Node
		// NodeModel[] nodes = Lufuse.NEP.getNMs();
		// int nNum = nodes.length;

		// 获取所有 Edge
		// EdgeModel[] edges = Lufuse.NEP.getEMs();
		// int eNum = edges.length;

		// 添加 聚类Node列表
		// for(int i = 0; i < nNum; i++){

		// }

		nodeTree.removeAll();
		edgeTree.removeAll();

		// 添加 Node 列表
		int nNum = gra.getNodeCount();

		for (int i = 0; i < nNum; i++) {

			Node node = gra.getNode(i);
			TreeItem item = new TreeItem(nodeTree, SWT.CHECK | SWT.MULTI);
			item.setText(i + " : " + node.getString("node_tag"));
			item.setData("node", gra.getNode(i));
			item.setChecked(true);
			// item.addListener(eventType, listener);
		}

		// 添加 Edge 列表
		int eNum = gra.getEdgeCount();
		for (int i = 0; i < eNum; i++) {
			Edge edge = gra.getEdge(i);
			TreeItem item = new TreeItem(edgeTree, SWT.CHECK);
			item.setText(i + " : N"
					+ edge.getSourceNode().getString("node_tag") + "----N"
					+ edge.getTargetNode().getString("node_tag") + " : "
					+ edge.getFloat("length"));
			item.setChecked(true);
		}

	}

	// 刷新视图
	private void refreshVis(Graph g) throws IOException {

		if (gra == null) {
			sss.append("错误！还没有导入视图！\n");
			return;
		}

		panel.removeAll();
		scs = new SnsCluShow(g);
		scs.setSize(panel.getSize());
		panel.add(scs);

	}

}
