package cn.edu.njau.lufuser;

import java.awt.Desktop;
import java.awt.Frame;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JPanel;

import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.wb.swt.SWTResourceManager;

import cn.edu.njau.lufuse.Lufuse;
import cn.edu.njau.lufuse.util.MyToolKit;
import cn.edu.njau.lufuse.vis.MaxViser;
import cn.edu.njau.lufuse.vis.Viser;
import cn.edu.njau.lufuser.dialog.DlgMatrixToXml;

import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;

import prefuse.data.Graph;
import prefuse.data.io.DataIOException;
import prefuse.data.io.GraphMLReader;
import prefuse.data.tuple.TableTuple;
import prefuse.data.tuple.TupleSet;

public class Lufuser2 {
	
	public static String PRO_NAME = "Lufuser";
	public static String PRO_VERSION = "0.2.4";
	public static String DEV_VERSION = "0.2.4.0116";
	public static String PRO_AUTHOR = "lujian863";
	public static String AUT_MAIL = "lujian863@qq.com";
	
	protected Shell shell;
	private Composite composite;
	private Frame myframe;
	private JPanel panel;
	private CTabFolder tabFolder;// 选项卡
	private StyledText sss;// 状态栏
	private Button startBtn;
	private Button fitBtn;
	
	private int cluId = 0;// 聚集编号
	
	private Display display;
	
	private Tree nodeTree;// Node 树
	private Tree edgeTree;// Edge 树
	private Tree aggrTree;// aggr 树
	
	private Viser dis = null;
	private MaxViser mdis = null;
	
	
	// 静态变量
	public static String PATH_EXCEL = "";// Excel 文件路径
	public static String PATH_CSV = "";// CSV 文件路径
	public static String PATH_XML = "";// Graph XML 文件路径
	
	
	public static void main(String[] args) {
		try {
			Lufuser2 window = new Lufuser2();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void open() {
		display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
	
	protected void createContents() {
		shell = new Shell();
		shell.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/mylogo-lf2.png"));
		shell.setSize(800, 600);
		shell.setText(Lufuser2.PRO_NAME+" "+Lufuser2.PRO_VERSION);
		
		setMidder();	// 设置居中显示
		addMenuBar();	// 添加菜单栏
		
		
		// 新建GridLayout布局
		GridLayout gridLayout = new GridLayout();
		// 把子组件分成2列显示
		gridLayout.numColumns = 2;
		shell.setLayout(gridLayout);
		
		GridData gridData;// 新建GridData
		gridData = new GridData(GridData.FILL_BOTH);// 两端填充
		gridData.horizontalSpan = 1;// GridData的组件占 1 列显示
		gridData.widthHint = 2000;// 最佳宽度
		gridData.heightHint = 1000;// 最佳高度

		composite = new Composite(shell, SWT.EMBEDDED);
		composite.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent e) {
				// composite重新设定大小之后
				//composite.setSize(600,530);
				print("Screen Size : " + (Integer.toString(composite.getSize().x) + " -- " +Integer.toString(composite.getSize().y)));
				// -5 是为了给边框预留显示空间
				myframe.setSize(composite.getSize().x - 5, composite.getSize().y - 5);
				panel.setSize(myframe.getSize());
				show();
			}
		});
		composite.setLayoutData(gridData);
		myframe = SWT_AWT.new_Frame(composite);
		panel = new JPanel();
		myframe.add(panel);
		
		// 添加右侧控制菜单
		gridLayout = new GridLayout();
		gridLayout.numColumns = 4;
		gridData = new GridData(GridData.VERTICAL_ALIGN_FILL);// 垂直填充
		gridData.horizontalSpan = 1;// GridData的组件占 1 列显示
		gridData.widthHint = 265;// 最佳宽度
		Group controlMenu = new Group(shell, SWT.NONE);
		controlMenu.setText("控制菜单");
		controlMenu.setLayout(gridLayout);
		controlMenu.setLayoutData(gridData);
		System.out.println(controlMenu.getSize());
		
		startBtn = new Button(controlMenu,SWT.NONE);
		startBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 重新绘图
				if(dis != null){
					dis.reDraw();
				}else{
					print("还未创建视图！");
				}
			}
		});
		startBtn.setText("\u91CD\u65B0\u7ED8\u56FE");
		fitBtn = new Button(controlMenu,SWT.NONE);
		fitBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 最佳绘图
				if(dis != null){
					dis.fix();
				}else{
					print("还未创建视图！");
				}
			}
		});
		fitBtn.setText("最佳显示");
		Button addCluBtn = new Button(controlMenu,SWT.NONE);
		addCluBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TreeItem item = new TreeItem(aggrTree, SWT.NONE);
				item.setText("聚集" + cluId);
				cluId++;
			}
		});
		addCluBtn.setText("添加聚集");
		
		Button btnAggr = new Button(controlMenu,SWT.NONE);
		btnAggr.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 刷新聚集
				if(dis != null){
					reAggrShow();
				}else{
					print("还未创建视图！");
				}
			}
		});
		btnAggr.setText("刷新聚集");
		
		
		// 添加选项卡
		gridData = new GridData(GridData.FILL_BOTH);// 两端填充
		gridData.horizontalSpan = 4;// GridData的组件占3 列显示
		//gridData.heightHint = 300;// 最佳高度
		
		tabFolder = new CTabFolder(controlMenu, SWT.BORDER);
		tabFolder.setLayoutData(gridData);
		CTabItem nodeTab = new CTabItem(tabFolder,SWT.NONE);
		nodeTab.setText("Nodes");
		tabFolder.setSelection(nodeTab);
		nodeTree = new Tree(tabFolder, SWT.BORDER | SWT.CHECK | SWT.V_SCROLL);
		nodeTree.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				print("Click!");
				//fishShow();
				// 检查是否有节点被反选
				TreeItem[] nodes = nodeTree.getItems();
				for(TreeItem node : nodes){
					if(node.getChecked() == false){
						print(node.getText());
						// 从 graph 中去除这些 node ，并重绘
						print(node.getData("node_id").toString());
						Lufuse.GRAPH_TMP.removeNode(Integer.parseInt(node.getData("node_id").toString()));
						//Lufuse.GRAPH_TMP.removeNode(1);
						//dis.removeNode(0);
						//show();
					}
				}
			}
		});
		nodeTab.setControl(nodeTree);
		
		CTabItem edgeTab = new CTabItem(tabFolder,SWT.NONE);
		edgeTab.setText("Edges");
		edgeTree = new Tree(tabFolder, SWT.BORDER | SWT.CHECK | SWT.V_SCROLL);
		edgeTab.setControl(edgeTree);
		
		//addEdgeTree();
		CTabItem aggrTab = new CTabItem(tabFolder,SWT.NONE);
		aggrTab.setText("Aggregate");
		aggrTree = new Tree(tabFolder, SWT.BORDER);
		aggrTab.setControl(aggrTree);
		
		
		
		// 添加状态栏
		gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL);// 水平填充
		gridData.horizontalSpan = 4;// GridData的组件占3 列显示
		gridData.heightHint = 100;// 最佳高度
		gridData.widthHint = 180;// 最佳宽度
		
		sss = new StyledText(controlMenu, SWT.BORDER | SWT.READ_ONLY | SWT.V_SCROLL | SWT.H_SCROLL);
		sss.setLayoutData(gridData);
		
		addDragControl();
		addPopupMenu();
	}
	
	// 添加菜单栏
	private void addMenuBar(){
		// 菜单
		Menu menu = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menu);
		
		MenuItem mnf = new MenuItem(menu, SWT.CASCADE);
		mnf.setText("\u6587\u4EF6");
		
		Menu mnfa = new Menu(mnf);
		mnf.setMenu(mnfa);
		
		MenuItem mnfx = new MenuItem(mnfa, SWT.NONE);
		mnfx.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 选择 XML 文件
				FileDialog fs = new FileDialog(shell, SWT.SINGLE);
				fs.setFilterNames(new String[] { "*.xml" });
				fs.setFilterExtensions(new String[] { "*.xml" });
				String fp = fs.open();
				if (fp == null) {
					print("未选择文件");
				} else {
					Lufuser2.PATH_XML = fp;
					int tmp = Lufuse.VIS_STATE;
					Lufuse.VIS_STATE = 1;
					print("已选择" + fp);
					print("正在加载视图...");
					getGraph();
					show();
					refreshTrees();
					print("视图加载成功！");
					Lufuse.VIS_STATE = tmp;
					show();
				}
			}
		});
		mnfx.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE_16x16-32.png"));
		mnfx.setText("\u5BFC\u5165XML\u6587\u4EF6");
		
		MenuItem mnfe = new MenuItem(mnfa, SWT.NONE);
		mnfe.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 退出
				MessageBox dlg = new MessageBox(shell, SWT.ICON_QUESTION | SWT.YES | SWT.NO);
				dlg.setMessage("确认退出 Lufuser ？");
				if(dlg.open() == SWT.YES){
                    shell.close();
                } 

			}
		});
		mnfe.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/POWER - STANDBY_16x16-32.png"));
		mnfe.setText("\u9000\u51FA");
		
		MenuItem mnc = new MenuItem(menu, SWT.CASCADE);
		mnc.setText("\u914D\u7F6E");
		
		Menu mnca = new Menu(mnc);
		mnc.setMenu(mnca);
		
		MenuItem mncv = new MenuItem(mnca, SWT.CASCADE);
		mncv.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mncv.setText("\u52A8\u9759\u6001\u8BBE\u7F6E");
		
		Menu menu_1 = new Menu(mncv);
		mncv.setMenu(menu_1);
		
		MenuItem mncvjing = new MenuItem(menu_1, SWT.NONE);
		mncvjing.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mncvjing.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.VIS_STATE = 0;
				show();
				print("静态显示");
			}
		});
		mncvjing.setText("\u9759\u6001\u663E\u793A");
		
		MenuItem mncvdong = new MenuItem(menu_1, SWT.NONE);
		mncvdong.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mncvdong.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lufuse.VIS_STATE = 1;
				show();
				print("动态显示");
			}
		});
		mncvdong.setText("\u52A8\u6001\u663E\u793A");
		
		MenuItem mncd = new MenuItem(mnca, SWT.CASCADE);
		mncd.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mncd.setText("Node \u663E\u793A");
		
		Menu mncaa = new Menu(mncd);
		mncd.setMenu(mncaa);
		
		MenuItem mncdi = new MenuItem(mncaa, SWT.NONE);
		mncdi.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Node 显示编号
				Lufuse.NODE_NAME = "node_id";
				show();
				print("Node 显示为 编号");
			}
		});
		mncdi.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mncdi.setText("\u663E\u793A\u7F16\u53F7");
		
		MenuItem mncdn = new MenuItem(mncaa, SWT.NONE);
		mncdn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Node 显示名称
				Lufuse.NODE_NAME = "node_tag";
				show();
				print("Node 显示为 名称");
			}
		});
		mncdn.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mncdn.setText("\u663E\u793A\u540D\u79F0");
		
		MenuItem mncf = new MenuItem(mnca, SWT.CASCADE);
		mncf.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - FONT_16x16-32.png"));
		mncf.setText("Node \u5B57\u4F53");
		
		Menu mncab = new Menu(mncf);
		mncf.setMenu(mncab);
		
		MenuItem mncfp3 = new MenuItem(mncab, SWT.NONE);
		mncfp3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 字体 +3
				Lufuse.NODE_FONT += 3;
				show();
				print("Node 字体已调整到 " + Lufuse.NODE_FONT);
			}
		});
		mncfp3.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - FONT_16x16-32.png"));
		mncfp3.setText("+3 px");
		
		MenuItem mncfm3 = new MenuItem(mncab, SWT.NONE);
		mncfm3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 字体 -3
				Lufuse.NODE_FONT -= 3;
				show();
				print("Node 字体已调整到 " + Lufuse.NODE_FONT);
			}
		});
		mncfm3.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - FONT_16x16-32.png"));
		mncfm3.setText("- 3 px");
		
		MenuItem mncf8 = new MenuItem(mncab, SWT.NONE);
		mncf8.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 字体 8
				Lufuse.NODE_FONT = 8;
				show();
				print("Node 字体已调整到 " + Lufuse.NODE_FONT);
			}
		});
		mncf8.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - FONT_16x16-32.png"));
		mncf8.setText("  8 px");
		
		MenuItem mncf12 = new MenuItem(mncab, SWT.NONE);
		mncf12.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 字体 12
				Lufuse.NODE_FONT = 12;
				show();
				print("Node 字体已调整到 " + Lufuse.NODE_FONT);
			}
		});
		mncf12.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - FONT_16x16-32.png"));
		mncf12.setText("12 px");
		
		MenuItem mncf16 = new MenuItem(mncab, SWT.NONE);
		mncf16.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 字体 16
				Lufuse.NODE_FONT = 16;
				show();
				print("Node 字体已调整到 " + Lufuse.NODE_FONT);
			}
		});
		mncf16.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - FONT_16x16-32.png"));
		mncf16.setText("16 px");
		
		MenuItem mncf20 = new MenuItem(mncab, SWT.NONE);
		mncf20.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 字体 20
				Lufuse.NODE_FONT = 20;
				show();
				print("Node 字体已调整到 " + Lufuse.NODE_FONT);
			}
		});
		mncf20.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - FONT_16x16-32.png"));
		mncf20.setText("20 px");
		
		MenuItem mncnl = new MenuItem(mnca, SWT.CASCADE);
		mncnl.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mncnl.setText("Node \u957F\u5EA6");
		
		Menu mncac = new Menu(mncnl);
		mncnl.setMenu(mncac);
		
		MenuItem mncnl50 = new MenuItem(mncac, SWT.NONE);
		mncnl50.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Node 宽度 50
				Lufuse.NODE_LENGTH = 50;
				show();
				print("Node 宽度已调整到 " + Lufuse.NODE_LENGTH);
			}
		});
		mncnl50.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mncnl50.setText("50 px");
		
		MenuItem mncnl80 = new MenuItem(mncac, SWT.NONE);
		mncnl80.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Node 宽度 80
				Lufuse.NODE_LENGTH = 80;
				show();
				print("Node 宽度已调整到 " + Lufuse.NODE_LENGTH);
			}
		});
		mncnl80.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mncnl80.setText("80 px");
		
		MenuItem mncnl100 = new MenuItem(mncac, SWT.NONE);
		mncnl100.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Node 宽度 100
				Lufuse.NODE_LENGTH = 100;
				show();
				print("Node 宽度已调整到 " + Lufuse.NODE_LENGTH);
			}
		});
		mncnl100.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mncnl100.setText("100 px");
		
		MenuItem mncnl150 = new MenuItem(mncac, SWT.NONE);
		mncnl150.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Node 宽度 150
				Lufuse.NODE_LENGTH = 150;
				show();
				print("Node 宽度已调整到 " + Lufuse.NODE_LENGTH);
			}
		});
		mncnl150.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mncnl150.setText("150 px");
		
		MenuItem mncnl300 = new MenuItem(mncac, SWT.NONE);
		mncnl300.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Node 宽度 300
				Lufuse.NODE_LENGTH = 300;
				show();
				print("Node 宽度已调整到 " + Lufuse.NODE_LENGTH);
			}
		});
		mncnl300.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mncnl300.setText("300 px");
		
		MenuItem mncef = new MenuItem(mnca, SWT.CASCADE);
		mncef.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/RAM_16x16-32.png"));
		mncef.setText("Edge \u7C97\u7EC6");
		
		Menu mncad = new Menu(mncef);
		mncef.setMenu(mncad);
		
		MenuItem mncef025 = new MenuItem(mncad, SWT.NONE);
		mncef025.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 粗细
				Lufuse.EDGE_BOUND = 0.25;
				show();
				print("Edge 粗细已调整到 " + Lufuse.EDGE_BOUND);
			}
		});
		mncef025.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/RAM_16x16-32.png"));
		mncef025.setText("0.25 px");
		
		MenuItem mncef050 = new MenuItem(mncad, SWT.NONE);
		mncef050.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 粗细
				Lufuse.EDGE_BOUND = 0.50;
				show();
				print("Edge 粗细已调整到 " + Lufuse.EDGE_BOUND);
			}
		});
		mncef050.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/RAM_16x16-32.png"));
		mncef050.setText("0.50 px");
		
		MenuItem mncef075 = new MenuItem(mncad, SWT.NONE);
		mncef075.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 粗细
				Lufuse.EDGE_BOUND = 0.75;
				show();
				print("Edge 粗细已调整到 " + Lufuse.EDGE_BOUND);
			}
		});
		mncef075.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/RAM_16x16-32.png"));
		mncef075.setText("0.75 px");
		
		MenuItem mncef100 = new MenuItem(mncad, SWT.NONE);
		mncef100.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 粗细
				Lufuse.EDGE_BOUND = 1.00;
				show();
				print("Edge 粗细已调整到 " + Lufuse.EDGE_BOUND);
			}
		});
		mncef100.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/RAM_16x16-32.png"));
		mncef100.setText("1.00 px");
		
		MenuItem mncef125 = new MenuItem(mncad, SWT.NONE);
		mncef125.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 粗细
				Lufuse.EDGE_BOUND = 1.25;
				show();
				print("Edge 粗细已调整到 " + Lufuse.EDGE_BOUND);
			}
		});
		mncef125.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/RAM_16x16-32.png"));
		mncef125.setText("1.25 px");
		
		MenuItem mncef150 = new MenuItem(mncad, SWT.NONE);
		mncef150.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 粗细
				Lufuse.EDGE_BOUND = 1.50;
				show();
				print("Edge 粗细已调整到 " + Lufuse.EDGE_BOUND);
			}
		});
		mncef150.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/RAM_16x16-32.png"));
		mncef150.setText("1.50 px");
		
		MenuItem mncef175 = new MenuItem(mncad, SWT.NONE);
		mncef175.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 粗细
				Lufuse.EDGE_BOUND = 1.75;
				show();
				print("Edge 粗细已调整到 " + Lufuse.EDGE_BOUND);
			}
		});
		mncef175.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/RAM_16x16-32.png"));
		mncef175.setText("1.75 px");
		
		MenuItem mncef200 = new MenuItem(mncad, SWT.NONE);
		mncef200.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 粗细
				Lufuse.EDGE_BOUND = 2.00;
				show();
				print("Edge 粗细已调整到 " + Lufuse.EDGE_BOUND);
			}
		});
		mncef200.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/RAM_16x16-32.png"));
		mncef200.setText("2.00 px");
		
		MenuItem mncel = new MenuItem(mnca, SWT.CASCADE);
		mncel.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mncel.setText("Edge \u957F\u5EA6");
		
		Menu mncae = new Menu(mncel);
		mncel.setMenu(mncae);
		
		MenuItem mncael05 = new MenuItem(mncae, SWT.NONE);
		mncael05.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 长度倍率
				Lufuse.EDGE_RATE = 0.5f;
				show();
				print("Edge 粗细已调整到 " + Lufuse.EDGE_BOUND);
			}
		});
		mncael05.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mncael05.setText("0.5 x");
		
		MenuItem mncael10 = new MenuItem(mncae, SWT.NONE);
		mncael10.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 长度倍率
				Lufuse.EDGE_RATE = 1.0f;
				show();
				print("Edge 长度倍率已调整到 " + Lufuse.EDGE_RATE);
			}
		});
		mncael10.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mncael10.setText("1.0 x");
		
		MenuItem mncael15 = new MenuItem(mncae, SWT.NONE);
		mncael15.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 长度倍率
				Lufuse.EDGE_RATE = 1.5f;
				show();
				print("Edge 长度倍率已调整到 " + Lufuse.EDGE_RATE);
			}
		});
		mncael15.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mncael15.setText("1.5 x");
		
		MenuItem mncael20 = new MenuItem(mncae, SWT.NONE);
		mncael20.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Edge 长度倍率
				Lufuse.EDGE_RATE = 2.0f;
				show();
				print("Edge 长度倍率已调整到 " + Lufuse.EDGE_RATE);
			}
		});
		mncael20.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mncael20.setText("2.0 x");
		
		MenuItem mncReset = new MenuItem(mnca, SWT.NONE);
		mncReset.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 恢复默认
				Lufuse.NODE_CONER		= 8;			// Node 的圆角
				Lufuse.NODE_MAX_W		= 80;			// Node 的最大长度
				Lufuse.NODE_NAME		= "node_id";	// Node 的名称
				Lufuse.NODE_FONT		= 10;			// Node 字体大小，关系到节点大小
				Lufuse.EDGE_RATE		= 1;			// Edge 倍率，多少倍
				Lufuse.NODE_LENGTH		= 80;			// Node 最大长度
				Lufuse.EDGE_BOUND		= 1.0;			// Edge 粗细
				Lufuse.VIS_STATE		= 1;			// 0 静态； 1 动态
				show();
				print("参数已恢复到默认状态！");
			}
		});
		mncReset.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/POWER - RESTART_16x16-32.png"));
		mncReset.setText("\u6062\u590D\u9ED8\u8BA4");
		
		MenuItem mnt = new MenuItem(menu, SWT.CASCADE);
		mnt.setText("\u5DE5\u5177");
		
		Menu mnta = new Menu(mnt);
		mnt.setMenu(mnta);
		
		MenuItem mntData = new MenuItem(mnta, SWT.NONE);
		mntData.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				DlgMatrixToXml dmx = new DlgMatrixToXml(shell, SWT.DIALOG_TRIM);
				dmx.open();
			}
		});
		mntData.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - BATCH_16x16-32.png"));
		mntData.setText("\u77E9\u9635\u8F6CXML");
		
		MenuItem mntRefreshVis = new MenuItem(mnta, SWT.NONE);
		mntRefreshVis.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				show();
			}
		});
		mntRefreshVis.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mntRefreshVis.setText("\u5237\u65B0\u89C6\u56FE");
		
		MenuItem mntRefreshTrees = new MenuItem(mnta, SWT.NONE);
		mntRefreshTrees.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 刷新数据列表
				refreshTrees();
			}
		});
		mntRefreshTrees.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mntRefreshTrees.setText("\u5237\u65B0\u6570\u636E\u5217\u8868");
		
		MenuItem mntNice = new MenuItem(mnta, SWT.NONE);
		mntNice.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 最佳显示
				if(dis != null){
					dis.fix();
				}else{
					print("还未创建视图！");
				}
			}
		});
		mntNice.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/FILE - PICTURE_16x16-32.png"));
		mntNice.setText("\u6700\u4F73\u663E\u793A");
		
		MenuItem mntAll = new MenuItem(mnta, SWT.NONE);
		mntAll.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 全屏
				if(dis != null){
					mdis = new MaxViser(dis);
					mdis.show();
				}else{
					print("还未创建视图，无法全屏！");
				}
			}
		});
		mntAll.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/NOTEBOOK_16x16-32.png"));
		mntAll.setText("\u5168\u5C4F\u663E\u793A");
		
		MenuItem mntPrint = new MenuItem(mnta, SWT.NONE);
		mntPrint.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/PHOTO_16x16-32.png"));
		mntPrint.setText("\u5C4F\u5E55\u622A\u56FE");
		
		MenuItem mnh = new MenuItem(menu, SWT.CASCADE);
		mnh.setText("\u5E2E\u52A9");
		
		Menu mnha = new Menu(mnh);
		mnh.setMenu(mnha);
		
		MenuItem mnhHelp = new MenuItem(mnha, SWT.NONE);
		mnhHelp.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 打开使用说明
				
			}
		});
		mnhHelp.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/CHAT_16x16-32.png"));
		mnhHelp.setText("\u4F7F\u7528\u8BF4\u660E");
		
		MenuItem mnhAbout = new MenuItem(mnha, SWT.NONE);
		mnhAbout.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 关于
				String abs = Lufuser2.PRO_NAME + " Version "
						+ Lufuser2.PRO_VERSION + "\nDev by "
						+ Lufuser2.PRO_AUTHOR + "\nE-mail : " 
						+ Lufuser2.AUT_MAIL + " \nDevVerion " 
						+ Lufuser2.DEV_VERSION;
				MessageBox messageBox = new MessageBox(shell,
						SWT.ICON_INFORMATION | SWT.OK);
				
				messageBox.setMessage(abs);
				print(abs);
				messageBox.open();
			}
		});
		mnhAbout.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/USER - M_16x16-32.png"));
		mnhAbout.setText("\u5173\u4E8E\u8F6F\u4EF6");
		
		MenuItem mnhUpdate = new MenuItem(mnha, SWT.NONE);
		mnhUpdate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 打开网页 http://lvdao.njau.edu.cn/lvdaosoft/lufuser/update/index.html
				try {  
			          
		            URI uri=new URI("http://lvdao.njau.edu.cn/lvdaosoft/lufuser/");  
		            Desktop.getDesktop().browse(uri);   
		  
		        } catch (IOException e1) {  
		            e1.printStackTrace();  
		        } catch (URISyntaxException e1) {  
		            e1.printStackTrace();  
		        }
			}
		});
		mnhUpdate.setImage(SWTResourceManager.getImage(Lufuser2.class, "/cn/edu/njau/lufuser/icons/INTERNET_16x16-32.png"));
		mnhUpdate.setText("\u68C0\u67E5\u66F4\u65B0");
	}
	
	// 设置窗体居中显示
	private void setMidder(){
		int scrWid = shell.getMonitor().getClientArea().width;
		int scrHei = shell.getMonitor().getClientArea().height;
		int x = shell.getSize().x;
		int y = shell.getSize().y;
		if (x > scrWid) {
			shell.getSize().x = scrWid;
		}
		if (y > scrHei) {
			shell.getSize().y = scrHei;
		}
		shell.setLocation((scrWid - x) / 2, (scrHei - y) / 2);
	}
	
	// 状态窗口输出函数
	private void print(String s){
		String time = MyToolKit.getSystemTime();
		sss.append(time + ": " + s +"\n");
		sss.setSelection(sss.getCharCount());// 跳转到最后一行
		System.out.println(s);
	}
	
	// 获取Graph
	private void getGraph(){
		Graph g = null;
		try {
			g = new GraphMLReader().readGraph(Lufuser2.PATH_XML);// 此步骤可能包括很多次的数据转换，这里读取一个XML文件
		} catch (DataIOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		Lufuse.GRAPH_TMP = g;
	}
	
	// 显示
	private void show(){
		if(Lufuser2.PATH_XML != "" && Lufuser2.PATH_XML != null){			
			try {
				dis = new Viser(Lufuse.GRAPH_TMP);
			} catch (IOException e) {
				e.printStackTrace();
			}
			panel.removeAll();
			dis.setSize(panel.getSize());
			panel.add(dis);
		}
	}
	
	// 刷新树
	private void refreshTrees(){
		this.cluId = 0;
		nodeShow();
		edgeShow();
		aggrShow();
	}
	
	// 显示 nodes 树
	@SuppressWarnings("rawtypes")
	private void nodeShow(){
		nodeTree.removeAll();
		// 获取所有 node
		Graph g = Lufuse.GRAPH_TMP;
		TupleSet nodes = g.getNodes();
		
		Iterator it = nodes.tuples();  
		while(it.hasNext()){  
			TableTuple node = (TableTuple) it.next();
            //System.out.println(node.get("node_tag"));
            TreeItem item = new TreeItem(nodeTree, SWT.CHECK);
            item.setText(node.getString("node_id")+"--"+node.getString("node_tag"));
            item.setData("node_id", node.getString("node_id"));
            item.setChecked(true);
//          item.addListener(, listener)
        }
	}
	
	// 显示 edges 树
	@SuppressWarnings("rawtypes")
	private void edgeShow(){
		edgeTree.removeAll();
		// 获取所有 edge
		Graph g = Lufuse.GRAPH_TMP;
		TupleSet edges = g.getEdges();
		
		Iterator it = edges.tuples();  
		while(it.hasNext()){  
			TableTuple edge = (TableTuple) it.next();
            TreeItem item = new TreeItem(edgeTree, SWT.CHECK);
            item.setText("["+edge.getString("source") +"--"+ edge.getString("target")+"] : "+edge.getString("length"));
            item.setChecked(true);
        }  

	}
	
	// 显示 aggr 树
	private void aggrShow(){
		aggrTree.removeAll();
		// 获取所有 node
		Graph g = Lufuse.GRAPH_TMP;
		TupleSet nodes = g.getNodes();
		
		for(int i = 0; i < 4; i++){
			TreeItem clus = new TreeItem(aggrTree, SWT.NONE);
			clus.setText("聚集"+cluId);
			cluId++;
		}
		TreeItem nodeList = new TreeItem(aggrTree, SWT.NONE);
		nodeList.setText("所有 Node");
		
		@SuppressWarnings("rawtypes")
		Iterator it = nodes.tuples();  
		while(it.hasNext()){  
			TableTuple node = (TableTuple) it.next();
            //System.out.println(node.get("node_tag"));
            TreeItem item = new TreeItem(nodeList, SWT.NONE);
            item.setText(node.getString("node_id")+"--"+node.getString("node_tag"));
            item.setData("node_id", node.getString("node_id"));
            item.setChecked(true);
            //item.addListener(eventType, listener)
        }
	}
	
	// add drag funciton 添加拖拽方法
	private void addDragControl(){
		Transfer[] types = new Transfer[] { TextTransfer.getInstance() };
		int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_LINK;
		
		// 建立拖拽 源————树tree对象
		final DragSource source = new DragSource(aggrTree, operations);
		source.setTransfer(types);
		final TreeItem[] dragSourceItem = new TreeItem[1];
		// 添加侦听器
		source.addDragListener(new DragSourceListener() {
			public void dragStart(DragSourceEvent event) {
				TreeItem[] selection = aggrTree.getSelection(); // 获取所选tree的节点
				// 判断所选节点是否存在并且是否为末端节点
				if (selection.length > 0 && selection[0].getItemCount() == 0){
					event.doit = true; // 启动拖拽功能
					dragSourceItem[0] = selection[0]; // 保存拖拽节点信息
				} else {
					event.doit = false;
				}
			};
			// 
			public void dragSetData(DragSourceEvent event) {
				String tmp = dragSourceItem[0].getText() + "|"+dragSourceItem[0].getData("node_id").toString();
				event.data = tmp; // 保存拖拽节点信息到事件数据中
				print("Node info : " + tmp);
			}
			// 
			public void dragFinished(DragSourceEvent event) {
				if (event.detail == DND.DROP_MOVE)
					dragSourceItem[0].dispose();
				dragSourceItem[0] = null;
			}
		});

		// 建立拖拽 目的————树tree对象
		DropTarget target = new DropTarget(aggrTree, operations);
		target.setTransfer(types);
		
		target.addDropListener(new DropTargetAdapter() {
			// 处理拖拽到目的上空时的方法
			public void dragOver(DropTargetEvent event) {
				event.feedback = DND.FEEDBACK_EXPAND | DND.FEEDBACK_SCROLL; // 响应拖拽动作，FEEDBACK_EXPAND表示拖拽到可以展开的节点时展开相应节点。
				if (event.item != null) {
					TreeItem item = (TreeItem) event.item;
					// 处理拖拽动作响应，根据拖拽的位置不同产生不同的响应
					Point pt = display.map(null, aggrTree, event.x, event.y); // 获取拖拽当前位置点
					Rectangle bounds = item.getBounds(); // 获取目的节点的边框

					// 根据拖拽当前位置点与目的节点的边框位置设置响应，如显示为FEEDBACK_INSERT_BEFORE、FEEDBACK_INSERT_AFTER
					if (pt.y < bounds.y + bounds.height / 3) {
						event.feedback |= DND.FEEDBACK_INSERT_BEFORE;
					} else if (pt.y > bounds.y + 2 * bounds.height / 3) {
						event.feedback |= DND.FEEDBACK_INSERT_AFTER;
					} else {
						event.feedback |= DND.FEEDBACK_SELECT;
					}
				}
			}

			// 处理放下时的方法
			public void drop(DropTargetEvent event) {
				if (event.data == null) {
					event.detail = DND.DROP_NONE;
					return;
				}
				String info = (String)event.data;
				String text = info.split("\\|")[0];
				int node_id = Integer.parseInt(info.split("\\|")[1]);
				print("获取Node ：" + text);
				print("获取Node ID：" + node_id);
				// 如果放下的位置没有节点
				if(event.item == null) {
					// 则新建立一个节点，以根节点为父节点
					TreeItem item = new TreeItem(aggrTree, SWT.NONE);
					item.setText(text);
					item.setData("node_id", node_id);
					print("当前移动节点ID为:"+node_id);
					print("无效移动！");
				}
				// 如果放下的位置有节点
				else{
					TreeItem item = (TreeItem) event.item;
					Point pt = display.map(null, aggrTree, event.x, event.y);
					Rectangle bounds = item.getBounds();
					TreeItem parent = item.getParentItem();
					// 没有父节点
					if (parent != null) {
						TreeItem[] items = parent.getItems();
						int index = 0;
						for (int i = 0; i < items.length; i++) {
							if (items[i] == item) {
								index = i;
								break;
							}
						}
						if (pt.y < bounds.y + bounds.height / 3) {
							// 以当前节点的父节点为父建立一个子节点，并且插入到当前节点前
							TreeItem newItem = new TreeItem(parent, SWT.NONE,index);
							newItem.setText(text);
							newItem.setData("node_id", node_id);
							print("当前移动节点ID为:"+node_id);
						} else if (pt.y > bounds.y + 2 * bounds.height / 3) {
							// 以当前节点的父节点为父建立一个子节点，并且插入到当前节点前
							TreeItem newItem = new TreeItem(parent, SWT.NONE,index + 1);
							newItem.setText(text);
							newItem.setData("node_id", node_id);
							print("当前移动节点ID为:"+node_id);
						} else {
							// 以当前为父节点建立一个子节点
							TreeItem newItem = new TreeItem(item, SWT.NONE);
							newItem.setData("node_id", node_id);
							print("当前移动节点ID为:"+node_id);
						}
					
						
					} else {	// 有父节点
						TreeItem[] items = aggrTree.getItems();
						int index = 0;
						for (int i = 0; i < items.length; i++) {
							if (items[i] == item) {
								index = i;
								break;
							}
						}
						if (pt.y < bounds.y + bounds.height / 3) {
							TreeItem newItem = new TreeItem(aggrTree, SWT.NONE,index);
							newItem.setText(text);
							newItem.setData("node_id", node_id);
							print("当前移动节点ID为:"+node_id);
						} else if (pt.y > bounds.y + 2 * bounds.height / 3) {
							TreeItem newItem = new TreeItem(aggrTree, SWT.NONE,index + 1);
							newItem.setText(text);
							newItem.setData("node_id", node_id);
							print("当前移动节点ID为:"+node_id);
						} else {
							TreeItem newItem = new TreeItem(item, SWT.NONE);
							newItem.setText(text);
							newItem.setData("node_id", node_id);
							print("当前移动节点ID为:"+node_id);
						}
					}
				}
			}
		});
	}
	
	private void reAggrShow(){
		// 有父节点的情况下，改变聚类效果
		List<int[]> clu = new ArrayList<int[]>();
		// 获取aggrTree
		TreeItem[] root = aggrTree.getItems();
		for(TreeItem tim : root){
			if(tim.getText() != "所有 Node" && tim.getItemCount() != 0){
				//print("有新类！");
				TreeItem[] citem = tim.getItems();
				int cLen = tim.getItemCount();
				int[] tmp = new int[cLen];
				for(int i = 0; i < cLen; i++){
					tmp[i] = Integer.parseInt(citem[i].getData("node_id").toString());
					//print("添加新元素!");
				}
				clu.add(tmp);
			}
		}
		// 刷新视图
		Lufuse.CLU_DATA = clu;
		show();
		//dis.reDraw();
		//dis.reDraw();
		// dis.fix();
	}
	
	private void addPopupMenu(){
		
		Menu menu = new Menu(aggrTree);
				aggrTree.setMenu(menu);
				
				MenuItem menuItem = new MenuItem(menu, SWT.NONE);
				menuItem.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						print("查看属性");
					}
				});
				menuItem.setText("\u5C5E\u6027");
			}
}