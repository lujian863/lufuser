package cn.edu.njau.lufuser.test;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class TTest {

	protected Shell shell;
	private Tree tree;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			TTest window = new TTest();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(494, 507);
		shell.setText("SWT Application");
		
		CTabFolder tabFolder = new CTabFolder(shell, SWT.BORDER);
		tabFolder.setBounds(10, 41, 358, 391);
		tabFolder.setSelectionBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));
		
		CTabItem tabItem = new CTabItem(tabFolder, SWT.NONE);
		tabItem.setText("New Item");
		
		tree = new Tree(tabFolder, SWT.BORDER | SWT.CHECK);
		tree.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				//
				//System.out.println("abc");
				// 获取所有子树
				TreeItem[] tmp = tree.getItems();
				
				// 遍历，判断集合状态，取消选中的触发相应事件
				for(int i = 0; i < tmp.length; i++){
					System.out.println();
				}
			}
		});
		tabItem.setControl(tree);
		
		TreeItem trtmNewTreeitem = new TreeItem(tree, SWT.CHECK);
		trtmNewTreeitem.setChecked(true);
		trtmNewTreeitem.setText("111");
		trtmNewTreeitem.setExpanded(true);
		
		TreeItem trtmNewTreeitem_2 = new TreeItem(tree, SWT.CHECK);
		trtmNewTreeitem_2.setChecked(true);
		trtmNewTreeitem_2.setText("222");
		trtmNewTreeitem_2.setExpanded(true);
		
		TreeItem trtmNewTreeitem_3 = new TreeItem(tree, SWT.CHECK);
		trtmNewTreeitem_3.setChecked(true);
		trtmNewTreeitem_3.setText("333");
		
		TreeItem trtmNewTreeitem_4 = new TreeItem(tree, SWT.CHECK);
		trtmNewTreeitem_4.setChecked(true);
		trtmNewTreeitem_4.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent arg0) {
				
//				System.out.println();
				
			}
		});
		trtmNewTreeitem_4.setText("444");
		
		TreeItem trtmNewTreeitem_1 = new TreeItem(tree, SWT.NONE);
		trtmNewTreeitem_1.setChecked(true);
		trtmNewTreeitem_1.setText("555");
		
		CTabItem tbtmNewItem = new CTabItem(tabFolder, SWT.NONE);
		tbtmNewItem.setText("New Item");

	}
}
