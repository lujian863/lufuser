package cn.edu.njau.lufuser.test;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import java.awt.GridLayout;

public class Test extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Test frame = new Test();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Test() {
		setTitle("Lufuser");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// 设置初始大小及窗体居中显示
		int scrWid = Toolkit.getDefaultToolkit().getScreenSize().width;
		int scrHei = Toolkit.getDefaultToolkit().getScreenSize().height;
		int x = 800;
		int y = 600;
		setBounds((scrWid - x) / 2, (scrHei - y) / 2, 800, 600);
		
		// 添加菜单条
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		// File Menu
		JMenu mFile = new JMenu(" File ");
		JMenuItem mifx = new JMenuItem("Import Data Source File ( Excel )");
		mFile.add(mifx);
		JMenuItem mifs = new JMenuItem("Import Data Source File ( CSV )");
		mFile.add(mifs);
		JMenuItem mifc = new JMenuItem("Exit Lufuser");
		mFile.add(mifc);
		menuBar.add(mFile);
	
		
		// Conf Menu
		JMenu mConf = new JMenu(" Conf ");
		menuBar.add(mConf);
		// node font
		JMenu mFont = new JMenu("Font Size");
		JMenuItem fs_add_3 = new JMenuItem(" + 3 px ");
		mFont.add(fs_add_3);
		JMenuItem fs_del_3 = new JMenuItem(" - 3 px ");
		mFont.add(fs_del_3);
		JMenuItem fs_8 = new JMenuItem(" 8 px ");
		mFont.add(fs_8);
		JMenuItem fs_12 = new JMenuItem(" 12 px ");
		mFont.add(fs_12);
		JMenuItem fs_16 = new JMenuItem(" 16 px ");
		mFont.add(fs_16);
		mConf.add(mFont);
		// node max length
		//JMenu m
		// edge length
		JMenu mELen = new JMenu("Edge Length");
		JMenuItem el_25x = new JMenuItem(" 0.25 x ");
		mELen.add(el_25x);
		JMenuItem el_05x = new JMenuItem(" 0.5 x ");
		mELen.add(el_05x);
		JMenuItem el_75x = new JMenuItem(" 0.75 x ");
		mELen.add(el_75x);
		JMenuItem el_1x = new JMenuItem(" 1 x ");
		mELen.add(el_1x);
		JMenuItem el_15x = new JMenuItem(" 1.5 x ");
		mELen.add(el_15x);
		JMenuItem el_2x = new JMenuItem(" 2 x ");
		mELen.add(el_2x);
		mConf.add(mELen);
		//
		
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		//新建GridLayout布局
        GridLayout layout = new GridLayout( );
        //把子组件分成3列显示
        layout.setColumns(3);
		contentPane.setLayout(layout);
		
		
		
		JPanel dis = new JPanel();
		dis.setBackground(Color.BLUE);
		contentPane.add(dis);
		
		//
		JTabbedPane jtp = new JTabbedPane(JTabbedPane.LEFT , JTabbedPane.WRAP_TAB_LAYOUT);
		contentPane.add(jtp);
		
		String[] tabs = {"Colors","Nodes","Edges"};
		for(String s : tabs){
			jtp.addTab(s, null);
		}
	}

}
