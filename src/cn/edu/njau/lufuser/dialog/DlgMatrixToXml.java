package cn.edu.njau.lufuser.dialog;

import java.io.IOException;

import jxl.read.biff.BiffException;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import cn.edu.njau.lufuse.Lufuse;
import cn.edu.njau.lufuse.algo.MatrixChanger;
import cn.edu.njau.lufuse.io.MatrixXlsReader;
import cn.edu.njau.lufuser.Lufuser2;

public class DlgMatrixToXml extends Dialog {

	protected Object result;
	protected Shell shell;
	private Text txtXlsPath;
	
	private Label sysSta;
	private ProgressBar proBar;
	private Text txtSavePath;
	private Button btnOutput;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public DlgMatrixToXml(Shell parent, int style) {
		super(parent, style);
		setText("矩阵转XML");
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), getStyle());
		shell.setSize(500, 200);
		shell.setText(getText());
		
		txtXlsPath = new Text(shell, SWT.BORDER);
		txtXlsPath.setBounds(10, 18, 280, 23);
		txtXlsPath.setText(Lufuser2.PATH_EXCEL);
		
		Button btnSelect = new Button(shell, SWT.NONE);
		btnSelect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 选择 Excel 文件
				FileDialog fs = new FileDialog(shell, SWT.SINGLE);
				fs.setText("选择Excel文件");
				fs.setFilterNames(new String[] { "*.xls" });
				fs.setFilterExtensions(new String[] { "*.xls" });
				String fp = fs.open();
				if (fp == null) {
					print("未选择文件");
				} else {
					Lufuser2.PATH_EXCEL = fp;
					txtXlsPath.setText(Lufuser2.PATH_EXCEL);
					print("已选择" + fp);
				}
			}
		});
		btnSelect.setBounds(296, 16, 98, 27);
		btnSelect.setText("\u9009\u62E9Excel\u6587\u4EF6");
		
		Button btnStart = new Button(shell, SWT.NONE);
		btnStart.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(txtXlsPath.getText().trim() == ""){
					print("请选择 Excel 文件！");
					return;
				}
				
				if(txtSavePath.getText().trim() == ""){
					print("请选择 XML 文件保存路径！");
					return;
				}
				
				proBar.setVisible(true);
				proBar.setSelection(20);
				// 读取xls文件数据
				String[][] mat = null;
				try {
					mat = MatrixXlsReader.getMatrixData(Lufuser2.PATH_EXCEL);
				} catch (BiffException e1) {
					print("错误:bif--" + e1.toString());
				} catch (IOException e1) {
					print("错误:io--" + e1.toString());
				}
				proBar.setSelection(50);
				MatrixChanger mc = new MatrixChanger(mat);
				proBar.setSelection(80);
				Lufuse.GRAPH_TMP = mc.matrixToGraph();
				print("数据预处理完成！");
				proBar.setSelection(100);
			}
		});
		btnStart.setBounds(400, 16, 80, 71);
		btnStart.setText("\u5F00\u59CB\u8F6C\u6362");
		
		proBar = new ProgressBar(shell, SWT.NONE);
		proBar.setBounds(10, 112, 470, 17);
		proBar.setMinimum(0);
		proBar.setMaximum(100);
		
		sysSta = new Label(shell, SWT.NONE);
		sysSta.setBounds(10, 145, 470, 17);
		sysSta.setText("\u72B6\u6001\uFF1A");
		
		txtSavePath = new Text(shell, SWT.BORDER);
		txtSavePath.setText("");
		txtSavePath.setBounds(10, 62, 280, 23);
		
		btnOutput = new Button(shell, SWT.NONE);
		btnOutput.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 选择导出路径
				DirectoryDialog folderdlg=new DirectoryDialog(shell,SWT.SINGLE);
				folderdlg.setText("选择XML文件保存路径");
				folderdlg.setFilterPath("SystemDrive");
				folderdlg.setMessage("请选择相应的文件夹");
				String selecteddir=folderdlg.open();
		        if(selecteddir==null){
		        	print("未选择文件夹"); 
		            return ;
		        }
		        else{
		            print("您选中的文件夹目录为："+selecteddir);
		            String dir = selecteddir+"\\graph.xml";
		            txtSavePath.setText(dir);
		            Lufuse.GRA_XML_PATH = dir;
		        }                    
			}
		});
		btnOutput.setText("\u9009\u62E9\u5BFC\u51FA\u8DEF\u5F84");
		btnOutput.setBounds(296, 60, 98, 27);
		setMidder();
	}
	
	private void setMidder(){
		//设置窗体居中显示
		int scrWid = shell.getMonitor().getClientArea().width; 
		int scrHei = shell.getMonitor().getClientArea().height;
		int x = shell.getSize().x;
		int y = shell.getSize().y;
		if(x > scrWid){
			shell.getSize().x = scrWid;
		}
		if(y > scrHei){
			shell.getSize().y = scrHei;
		}
		shell.setLocation((scrWid - x)/2, (scrHei - y)/2);
	}
	
	private void print(String str){
		sysSta.setText("状态：" + str); 
	}
}
