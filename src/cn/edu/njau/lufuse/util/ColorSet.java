package cn.edu.njau.lufuse.util;

import prefuse.util.ColorLib;

public class ColorSet {

	// 根据输入的数字生成对应数量的具有较高区分度的颜色
	// 目前这个还没写好，生成的颜色不是很好
	public static int[] getDiffColors(int cNum) {

		// 先生成cNum个rgb
		int r_mid = 255 / cNum;
		int g_mid = 255 / cNum;
		int b_mid = 255 / cNum;

		int[] cCol = new int[cNum];

		for (int i = 0; i < cNum; i++) {
			cCol[i] = ColorLib.rgb(r_mid * i, g_mid * i, b_mid * i);
		}

		cCol[0] = ColorLib.rgb(202, 20, 20);
		cCol[1] = ColorLib.rgb(20, 20, 20);

		return cCol;
	}

	// 建一个颜色库，需要几种颜色就从里面获取
	public static int[] getBackgroundColors(int cNum) {

		int r = 150;
		int g = 150;
		int b = 150;
		int a = 150;

		int[] cCol = new int[cNum];

		for (int i = 0; i < cNum; i++) {
			int mod = i % 4;
			switch (mod) {
			case 0:
				cCol[i] = ColorLib.rgba(r + i * 50, g, b, a);
			case 1:
				cCol[i] = ColorLib.rgba(r, g + i * 50, b, a);
			case 2:
				cCol[i] = ColorLib.rgba(r, g, b + i * 50, a);
			case 3:
				cCol[i] = ColorLib.rgba(r, g, b, a + i * 50);
			}

		}

		return cCol;
	}

	public static int[] getBackLib(int num) {

		int[] lib = new int[] { ColorLib.rgba(255, 200, 200, 150),
				ColorLib.rgba(200, 255, 200, 150),
				ColorLib.rgba(200, 200, 255, 150),
				ColorLib.rgba(200, 200, 200, 150),
				ColorLib.rgba(255, 255, 255, 150),
				ColorLib.rgba(200, 255, 255, 150) };

		int[] cols;

		if (num >= lib.length) {
			cols = lib;
		} else {
			cols = new int[num];
			for (int i = 0; i < num; i++) {
				cols[i] = lib[i];
			}
		}
		return cols;
	}

}