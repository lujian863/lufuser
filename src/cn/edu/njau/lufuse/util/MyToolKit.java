package cn.edu.njau.lufuse.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyToolKit {

	// 获取操作系统当前时间
	public static String getSystemTime(){
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd HH:mm:ss");//设置日期格式
		String t = df.format(new Date()).toString();
		return t;
	}
	
}
