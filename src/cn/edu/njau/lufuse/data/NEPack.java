package cn.edu.njau.lufuse.data;

// ��װEdgeModel[] �� NodeModel[]
public class NEPack {
	
	private NodeModel[] nms;
	private EdgeModel[] ems;
	
	public NEPack(NodeModel[] nms, EdgeModel[] ems){
		this.setNMs(nms);
		this.setEMs(ems);
	}

	public NodeModel[] getNMs() {
		return nms;
	}

	public void setNMs(NodeModel[] nms) {
		this.nms = nms;
	}

	public EdgeModel[] getEMs() {
		return ems;
	}

	public void setEMs(EdgeModel[] ems) {
		this.ems = ems;
	}
}
