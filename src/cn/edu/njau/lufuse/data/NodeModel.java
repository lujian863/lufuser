package cn.edu.njau.lufuse.data;

public class NodeModel {
	private String	tags;	// Node节点显示的文字
	private int		nodeId;		// NodeID
	private String	colorId;	// 颜色ID，默认为1

	public NodeModel(String tags, int nid) {
		this.setTags(tags);
		this.setNodeId(nid);
		this.setColorId("1");
	}

	public NodeModel(String tags, int nid, String cid) {
		this.setTags(tags);
		this.setNodeId(nid);
		this.setColorId(cid);
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public int getNodeId() {
		return nodeId;
	}

	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}

	public String getColorId() {
		return colorId;
	}

	public void setColorId(String colorId) {
		this.colorId = colorId;
	}
}
