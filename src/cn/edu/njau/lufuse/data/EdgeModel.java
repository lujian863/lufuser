package cn.edu.njau.lufuse.data;

public class EdgeModel {
	private int sourceNodeId;	// 源节点
	private int targetNodeId;	// 目标节点
	private float length;		// Edge长度

	public EdgeModel(int sid, int tid, float len) {
		this.setSourceNodeId(sid);
		this.setTargetNodeId(tid);
		this.setLength(len);
	}

	public int getSourceNodeId() {
		return sourceNodeId;
	}

	public void setSourceNodeId(int sourceNodeId) {
		this.sourceNodeId = sourceNodeId;
	}

	public int getTargetNodeId() {
		return targetNodeId;
	}

	public void setTargetNodeId(int targetNodeId) {
		this.targetNodeId = targetNodeId;
	}

	public float getLength() {
		return length;
	}

	public void setLength(float length) {
		this.length = length;
	}

}
