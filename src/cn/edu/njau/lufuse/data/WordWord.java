package cn.edu.njau.lufuse.data;

public class WordWord {
	private String word_one;
	private String word_two;
	
	public WordWord(String w1, String w2){
		this.setWord_one(w1);
		this.setWord_two(w2);
	}

	public String getWord_one() {
		return word_one;
	}

	public void setWord_one(String word_one) {
		this.word_one = word_one;
	}

	public String getWord_two() {
		return word_two;
	}

	public void setWord_two(String word_two) {
		this.word_two = word_two;
	}
}
