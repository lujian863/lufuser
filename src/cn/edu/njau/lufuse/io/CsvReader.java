package cn.edu.njau.lufuse.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;

public class CsvReader {

	// csv格式聚类数据读取
	public static List<int[]> getCsvCluster(String path) {
		List<String[]> fcontent = null;
		try {
			File f = new File(path);
			CSVReader cr = new CSVReader(new FileReader(f));
			fcontent = cr.readAll();
		} catch (FileNotFoundException e) {
			System.out.println("CSV文件未找到！");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("CSV文件读取出错！");
			e.printStackTrace();
		}

		int listLen = fcontent.size();
		List<int[]> rs = new ArrayList<int[]>();
		for(int i = 0; i < listLen; i++){
			String[] s = fcontent.get(i);
			int sLen = s.length;
			int[] tmp = new int[sLen];
			for(int j = 0; j < sLen; j++){
				tmp[j] = Integer.parseInt(s[j]);
			}
			rs.add(tmp);
		}
		return rs;
	}
	
	// csv格式矩阵读取
	public static String[][] getCsvMatrix(String path){
		
		return new String[0][0];
	}

}
