package cn.edu.njau.lufuse.io;

import java.io.File;
import java.io.IOException;

import cn.edu.njau.lufuse.Lufuse;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;

// xls格式矩阵读取类
public class MatrixXlsReader {

	public static String[][] getMatrixData(String fpath) throws BiffException,IOException {
		WorkbookSettings wss = new WorkbookSettings();
		wss.setEncoding(Lufuse.XLS_ENCODING);// 文件编码，乱码问题从这里解决
		Workbook book = Workbook.getWorkbook(new File(fpath), wss);

		// 获得第一个工作表对象
		Sheet sheet = book.getSheet(0);
		int colNum = sheet.getColumns(); // 得到列数
		int rowNum = sheet.getRows(); // 得到行数

		System.out.println("文件"+ fpath +"共有 ：" + colNum + "列");
		System.out.println("文件"+ fpath +"共有 ：" + rowNum + "行");

		String[][] data = new String[rowNum][colNum];

		for (int i = 0; i < rowNum; i++) {
			for (int j = 0; j < colNum; j++) {
				Cell ce = sheet.getCell(j, i);
				String rs = ce.getContents().trim();
				data[i][j] = rs;
			}
		}
		book.close();
		return data;
	}
}
