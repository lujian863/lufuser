package cn.edu.njau.lufuse.io;

import cn.edu.njau.lufuse.Lufuse;
import cn.edu.njau.lufuse.data.EdgeModel;
import cn.edu.njau.lufuse.data.NEPack;
import cn.edu.njau.lufuse.data.NodeModel;
import prefuse.data.Graph;
import prefuse.data.Node;
import prefuse.data.Table;
import prefuse.data.io.DataIOException;
import prefuse.data.io.GraphMLWriter;

public class GraphXmlWriter {
	
	// 将 NEPack 转化为 GraphXML
	public static Graph transNepToXml(NEPack nep) throws DataIOException{
		
		// 解包 NEPack
		NodeModel[] nodeArr = nep.getNMs();
		EdgeModel[] edgeArr = nep.getEMs();
		
		// 建立 Graph 对象
		Graph gra = new Graph();
		GraphMLWriter gw = new GraphMLWriter();
		
		// 添加 Node 字段元素
		Table m = gra.getNodeTable();
		m.addColumn("node_id", String.class);
		m.addColumn("node_tag", String.class);
		m.addColumn("color_id", String.class);
		
		// 添加 Node
		int nodeLen = nodeArr.length;
		for (int i = 0; i < nodeLen; i++) {
			Node n = gra.addNode();
			n.set("node_id", Integer.toString(nodeArr[i].getNodeId()));
			n.set("node_tag", nodeArr[i].getTags());
			n.set("color_id", "1");
		}
		
		// 添加 Edge 字段元素
		Table e = gra.getEdgeTable();
		e.addColumn("length", float.class);
		
		// 添加边
		int edgeLen = edgeArr.length;
		for (int i = 0; i < edgeLen; i++) {
			int x = edgeArr[i].getSourceNodeId();
			int y = edgeArr[i].getTargetNodeId();
			int j = gra.addEdge(x, y);
			gra.getEdge(j).setFloat("length", edgeArr[i].getLength());
		}
		
		// 写入并返回Graph
		gw.writeGraph(gra,Lufuse.GRA_XML_PATH);
		return gra;
	}
}
