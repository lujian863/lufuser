package cn.edu.njau.lufuse.vis;

import java.awt.geom.Rectangle2D;

import prefuse.Display;
import prefuse.Visualization;
import prefuse.util.GraphicsLib;
import prefuse.util.display.DisplayLib;

public class FixToScreen {
	private long m_duration = 2000;
	private int m_margin = 50;
	private String m_group;
	private Display display;

	public FixToScreen(Display display) {
		this.display = display;
		m_group = Visualization.ALL_ITEMS;
	}

	public void fix() {
		Visualization vis = display.getVisualization();
		Rectangle2D bounds = vis.getBounds(m_group);
		GraphicsLib.expand(bounds, m_margin + (int) (1 / display.getScale()));
		DisplayLib.fitViewToBounds(display, bounds, m_duration);
		vis.repaint();
		System.out.println("û���⡣��������");
	}
}
