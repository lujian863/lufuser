package cn.edu.njau.lufuse.vis;

import java.awt.geom.Rectangle2D;

import prefuse.Display;
import prefuse.Visualization;
import prefuse.util.GraphicsLib;
import prefuse.util.display.DisplayLib;

public class FixAllShow implements Runnable {
	private long m_duration = 500;// 持续时间
	private int m_margin = 50;
	private String m_group;
	private Display display;

	public FixAllShow(Display display) {
		this.display = display;
		m_group = Visualization.ALL_ITEMS;
	}

	@Override
	public void run() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("适应屏幕调整~");
		Visualization vis = display.getVisualization();
		Rectangle2D bounds = vis.getBounds(m_group);
		GraphicsLib.expand(bounds, m_margin + (int) (1 / display.getScale()));
		DisplayLib.fitViewToBounds(display, bounds, m_duration);
		
		System.out.println("固定整个vis:");
		vis.repaint();
		//display.removeAncestorListener(r);		
	}
}
