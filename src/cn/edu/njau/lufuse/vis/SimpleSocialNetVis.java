package cn.edu.njau.lufuse.vis;

import javax.swing.JFrame;

import cn.edu.njau.lufuse.util.ColorSet;

import prefuse.Constants;
import prefuse.Display;
import prefuse.Visualization;
import prefuse.action.ActionList;
import prefuse.action.RepaintAction;
import prefuse.action.assignment.ColorAction;
import prefuse.action.assignment.DataColorAction;
import prefuse.action.layout.graph.ForceDirectedLayout;
import prefuse.activity.Activity;
import prefuse.controls.DragControl;
import prefuse.controls.PanControl;
import prefuse.controls.ZoomControl;
import prefuse.data.Graph;
import prefuse.data.io.DataIOException;
import prefuse.data.io.GraphMLReader;
import prefuse.render.AxisRenderer;
import prefuse.render.DefaultRendererFactory;
import prefuse.render.EdgeRenderer;
import prefuse.render.LabelRenderer;
import prefuse.render.PolygonRenderer;
import prefuse.render.Renderer;
import prefuse.render.ShapeRenderer;
import prefuse.util.ColorLib;
import prefuse.visual.VisualItem;

public class SimpleSocialNetVis {

	// 通过XML文件展示
	public static void showByXml(String xmlPath) {
		Graph graph = null;// 将可视化化需要的数据读入prefuse内部的数据结构，此例选择的是内部的graph结构
		try {
			graph = new GraphMLReader().readGraph(xmlPath);// 此步骤可能包括很多次的数据转换
		} catch (DataIOException e) {
			e.printStackTrace();
			System.err.println("Error");
		}
		showByGraph(graph);
	}

	// 直接通过Graph展示
	public static void showByGraph(Graph graph) {
		// 创建一个可视化图的抽象概念（内部存储的数据结构）
		Visualization vis = new Visualization();// 此结构包括原始数据域和新的可视化信息，如：x,y坐标，颜色，大小
		vis.add("graph", graph);

		// render和render工厂，用来传递数据
		//LabelRenderer r = new LabelRenderer("node_tag");// 使用name来创建带有标签的节点
		//r.setRoundedCorner(8, 8);
		LabelRenderer nodeR = new LabelRenderer("node_tag");
		nodeR.setMaxTextWidth(60);
		nodeR.setRoundedCorner(5, 5);
		nodeR.setVerticalAlignment(0);
		//nodeR.setTextField("adsadfasdf");
		//Renderer polyR = new PolygonRenderer(Constants.POLY_TYPE_CURVE);
        //((PolygonRenderer)polyR).setCurveSlack(0.15f);
		EdgeRenderer er = new EdgeRenderer(Constants.EDGE_TYPE_LINE,Constants.EDGE_ARROW_REVERSE);
		er.setArrowHeadSize(10, 10);
		
		DefaultRendererFactory drf = new DefaultRendererFactory();
        drf.setDefaultRenderer(nodeR);
        drf.setDefaultEdgeRenderer(er);
        vis.setRendererFactory(drf);
        
       // Renderer edgeR = new EdgeRenderer(1);
       // drf.setDefaultEdgeRenderer(edgeR);
        // drf.add("ingroup('aggregates')", polyR);
        // m_vis.setRendererFactory(drf);
        
        
		//vis.setRendererFactory(new DefaultRendererFactory(r));// 决定图形怎么画的主要工具

		// 数据处理动作，高于前面的visualization处理
		int[] allColors = ColorSet.getDiffColors(5);
		DataColorAction fill = new DataColorAction("graph.nodes", "color_id",
				Constants.NOMINAL, VisualItem.FILLCOLOR, allColors);

		ColorAction text = new ColorAction("graph.nodes", VisualItem.TEXTCOLOR,
				ColorLib.gray(0));

		ColorAction edges = new ColorAction("graph.edges",
				VisualItem.STROKECOLOR, ColorLib.gray(0));

		ActionList color = new ActionList();// 用来将前面的数据处理动作集合在一起
		color.add(fill);
		color.add(text);
		color.add(edges);

		ActionList layout = new ActionList(Activity.INFINITY);
		//layout.add(new ForceDirectedLayout("graph"));
		layout.add(new EdgeForceDirectedLayout("graph",true));
		layout.add(new RepaintAction());
		vis.putAction("color", color);
		vis.putAction("layout", layout);

		Display display = new Display(vis);
		display.setSize(800, 600);
		display.pan(300, 300);
		display.setHighQuality(true);
		display.addControlListener(new DragControl());
		display.addControlListener(new PanControl());
		display.addControlListener(new ZoomControl());

		
		JFrame frame = new JFrame("Test2");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(display); 
		frame.pack(); 
		frame.setVisible(true); 

		vis.run("color");
		vis.run("layout");

	}
	
	public static Display getDispalyByXml(String xmlPath) {
		Graph graph = null;// 将可视化化需要的数据读入prefuse内部的数据结构，此例选择的是内部的graph结构
		try {
			graph = new GraphMLReader().readGraph(xmlPath);// 此步骤可能包括很多次的数据转换
		} catch (DataIOException e) {
			e.printStackTrace();
			System.err.println("Erroe");
		}
		return getDispalyByGraph(graph);
	}

	public static Display getDispalyByGraph(Graph graph) {
		
		Visualization vis = new Visualization();// 此结构包括原始数据域和新的可视化信息，如：x,y坐标，颜色，大小
		vis.add("graph", graph);

		// render和render工厂，用来传递数据
		//LabelRenderer r = new LabelRenderer("node_tag");// 使用name来创建带有标签的节点
		//r.setRoundedCorner(8, 8);
		Renderer r = new ShapeRenderer(20);
		
		vis.setRendererFactory(new DefaultRendererFactory(r));// 决定图形怎么画的主要工具

		// 数据处理动作，高于前面的visualization处理
		int[] allColors = ColorSet.getDiffColors(5);
		DataColorAction fill = new DataColorAction("graph.nodes", "color_id",
				Constants.NOMINAL, VisualItem.FILLCOLOR, allColors);

		ColorAction text = new ColorAction("graph.nodes", VisualItem.TEXTCOLOR,
				ColorLib.gray(0));

		ColorAction edges = new ColorAction("graph.edges",
				VisualItem.STROKECOLOR, ColorLib.gray(200));

		ActionList color = new ActionList();// 用来将前面的数据处理动作集合在一起
		color.add(fill);
		color.add(text);
		color.add(edges);

		ActionList layout = new ActionList(Activity.INFINITY);
		layout.add(new ForceDirectedLayout("graph"));
		layout.add(new RepaintAction());
		vis.putAction("color", color);
		vis.putAction("layout", layout);

		Display display = new Display(vis);
		display.setSize(800, 500);
		display.addControlListener(new DragControl());
		display.addControlListener(new PanControl());
		display.addControlListener(new ZoomControl());

		vis.run("color");
		vis.run("layout");

		return display;
	}
}
