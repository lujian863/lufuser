package cn.edu.njau.lufuse.vis;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RectangularShape;

import prefuse.Constants;
import prefuse.render.AbstractShapeRenderer;
import prefuse.util.FontLib;
import prefuse.util.StringLib;
import prefuse.visual.VisualItem;

public class PointTextRenderer extends AbstractShapeRenderer {

	private int m_baseSize = 10;
	protected String m_labelName = "label";
	protected String m_delim = "\n";

	private Ellipse2D m_ellipse = new Ellipse2D.Double();
	private Rectangle2D m_rect = new Rectangle2D.Double();
	private GeneralPath m_path = new GeneralPath();

	protected int m_xAlign = Constants.CENTER;
	protected int m_yAlign = Constants.CENTER;
	protected int m_hTextAlign = Constants.CENTER;
	protected int m_vTextAlign = Constants.CENTER;
	protected int m_hImageAlign = Constants.CENTER;
	protected int m_vImageAlign = Constants.CENTER;
	protected int m_imagePos = Constants.LEFT;

	protected int m_horizBorder = 2;
	protected int m_vertBorder = 0;
	protected int m_imageMargin = 2;
	protected int m_arcWidth = 0;
	protected int m_arcHeight = 0;

	protected int m_maxTextWidth = -1;

	/** The holder for the currently computed bounding box */
	protected RectangularShape m_bbox = new Rectangle2D.Double();
	protected Point2D m_pt = new Point2D.Double(); // temp point
	protected Font m_font; // temp font holder
	protected String m_text; // label text
	protected Dimension m_textDim = new Dimension(); // text width / height

	public PointTextRenderer(int size, String textField) {
		this.setBaseSize(size);
		this.setTextField(textField);
	}

	private void setBaseSize(int size) {
		m_baseSize = size;

	}

	private void setTextField(String textField) {
		m_labelName = textField;
	}

	protected String getText(VisualItem item) {
		String s = null;
		if (item.canGetString(m_labelName)) {
			return item.getString(m_labelName);
		}
		return s;
	}

	protected Shape getRawShape(VisualItem item) {
		m_text = getText(item);
		double size = item.getSize();
		// get text dimensions
		int tw = 0, th = 0;
		if (m_text != null) {
			m_text = computeTextDimensions(item, m_text, size);
			th = m_textDim.height;
			tw = m_textDim.width;
		}
		// get bounding box dimensions
		double w = 0, h = 0;
		

		int stype = item.getShape();
		double x = item.getX();
		if (Double.isNaN(x) || Double.isInfinite(x))
			x = 0;
		double y = item.getY();
		if (Double.isNaN(y) || Double.isInfinite(y))
			y = 0;
		double width = m_baseSize * item.getSize();

		// Center the shape around the specified x and y
		if (width > 1) {
			x = x - width / 2;
			y = y - width / 2;
		}

		switch (stype) {
		case Constants.SHAPE_NONE:
			return null;
		case Constants.SHAPE_RECTANGLE:
			return rectangle(x, y, width, width);
		case Constants.SHAPE_ELLIPSE:
			return ellipse(x, y, width, width);
		case Constants.SHAPE_TRIANGLE_UP:
			return triangle_up((float) x, (float) y, (float) width);
		case Constants.SHAPE_TRIANGLE_DOWN:
			return triangle_down((float) x, (float) y, (float) width);
		case Constants.SHAPE_TRIANGLE_LEFT:
			return triangle_left((float) x, (float) y, (float) width);
		case Constants.SHAPE_TRIANGLE_RIGHT:
			return triangle_right((float) x, (float) y, (float) width);
		case Constants.SHAPE_CROSS:
			return cross((float) x, (float) y, (float) width);
		case Constants.SHAPE_STAR:
			return star((float) x, (float) y, (float) width);
		case Constants.SHAPE_HEXAGON:
			return hexagon((float) x, (float) y, (float) width);
		case Constants.SHAPE_DIAMOND:
			return diamond((float) x, (float) y, (float) width);
		default:
			throw new IllegalStateException("Unknown shape type: " + stype);
		}
	}

	public Shape rectangle(double x, double y, double width, double height) {
		m_rect.setFrame(x, y, width, height);
		return m_rect;
	}

	public Shape ellipse(double x, double y, double width, double height) {
		m_ellipse.setFrame(x, y, width, height);
		return m_ellipse;
	}

	/**
	 * Returns a up-pointing triangle of the given dimenisions.
	 */
	public Shape triangle_up(float x, float y, float height) {
		m_path.reset();
		m_path.moveTo(x, y + height);
		m_path.lineTo(x + height / 2, y);
		m_path.lineTo(x + height, (y + height));
		m_path.closePath();
		return m_path;
	}

	/**
	 * Returns a down-pointing triangle of the given dimenisions.
	 */
	public Shape triangle_down(float x, float y, float height) {
		m_path.reset();
		m_path.moveTo(x, y);
		m_path.lineTo(x + height, y);
		m_path.lineTo(x + height / 2, (y + height));
		m_path.closePath();
		return m_path;
	}

	/**
	 * Returns a left-pointing triangle of the given dimenisions.
	 */
	public Shape triangle_left(float x, float y, float height) {
		m_path.reset();
		m_path.moveTo(x + height, y);
		m_path.lineTo(x + height, y + height);
		m_path.lineTo(x, y + height / 2);
		m_path.closePath();
		return m_path;
	}

	/**
	 * Returns a right-pointing triangle of the given dimenisions.
	 */
	public Shape triangle_right(float x, float y, float height) {
		m_path.reset();
		m_path.moveTo(x, y + height);
		m_path.lineTo(x + height, y + height / 2);
		m_path.lineTo(x, y);
		m_path.closePath();
		return m_path;
	}

	/**
	 * Returns a cross shape of the given dimenisions.
	 */
	public Shape cross(float x, float y, float height) {
		float h14 = 3 * height / 8, h34 = 5 * height / 8;
		m_path.reset();
		m_path.moveTo(x + h14, y);
		m_path.lineTo(x + h34, y);
		m_path.lineTo(x + h34, y + h14);
		m_path.lineTo(x + height, y + h14);
		m_path.lineTo(x + height, y + h34);
		m_path.lineTo(x + h34, y + h34);
		m_path.lineTo(x + h34, y + height);
		m_path.lineTo(x + h14, y + height);
		m_path.lineTo(x + h14, y + h34);
		m_path.lineTo(x, y + h34);
		m_path.lineTo(x, y + h14);
		m_path.lineTo(x + h14, y + h14);
		m_path.closePath();
		return m_path;
	}

	/**
	 * Returns a star shape of the given dimenisions.
	 */
	public Shape star(float x, float y, float height) {
		float s = (float) (height / (2 * Math.sin(Math.toRadians(54))));
		float shortSide = (float) (height / (2 * Math.tan(Math.toRadians(54))));
		float mediumSide = (float) (s * Math.sin(Math.toRadians(18)));
		float longSide = (float) (s * Math.cos(Math.toRadians(18)));
		float innerLongSide = (float) (s / (2 * Math.cos(Math.toRadians(36))));
		float innerShortSide = innerLongSide
				* (float) Math.sin(Math.toRadians(36));
		float innerMediumSide = innerLongSide
				* (float) Math.cos(Math.toRadians(36));

		m_path.reset();
		m_path.moveTo(x, y + shortSide);
		m_path.lineTo((x + innerLongSide), (y + shortSide));
		m_path.lineTo((x + height / 2), y);
		m_path.lineTo((x + height - innerLongSide), (y + shortSide));
		m_path.lineTo((x + height), (y + shortSide));
		m_path.lineTo((x + height - innerMediumSide),
				(y + shortSide + innerShortSide));
		m_path.lineTo((x + height - mediumSide), (y + height));
		m_path.lineTo((x + height / 2),
				(y + shortSide + longSide - innerShortSide));
		m_path.lineTo((x + mediumSide), (y + height));
		m_path.lineTo((x + innerMediumSide), (y + shortSide + innerShortSide));
		m_path.closePath();
		return m_path;
	}

	/**
	 * Returns a hexagon shape of the given dimenisions.
	 */
	public Shape hexagon(float x, float y, float height) {
		float width = height / 2;

		m_path.reset();
		m_path.moveTo(x, y + 0.5f * height);
		m_path.lineTo(x + 0.5f * width, y);
		m_path.lineTo(x + 1.5f * width, y);
		m_path.lineTo(x + 2.0f * width, y + 0.5f * height);
		m_path.lineTo(x + 1.5f * width, y + height);
		m_path.lineTo(x + 0.5f * width, y + height);
		m_path.closePath();
		return m_path;
	}

	/**
	 * Returns a diamond shape of the given dimenisions.
	 */
	public Shape diamond(float x, float y, float height) {
		m_path.reset();
		m_path.moveTo(x, (y + 0.5f * height));
		m_path.lineTo((x + 0.5f * height), y);
		m_path.lineTo((x + height), (y + 0.5f * height));
		m_path.lineTo((x + 0.5f * height), (y + height));
		m_path.closePath();
		return m_path;
	}

	private String computeTextDimensions(VisualItem item, String text,
			double size) {
		// put item font in temp member variable
		m_font = item.getFont();
		// scale the font as needed
		if (size != 1) {
			m_font = FontLib.getFont(m_font.getName(), m_font.getStyle(), size
					* m_font.getSize());
		}

		FontMetrics fm = DEFAULT_GRAPHICS.getFontMetrics(m_font);
		StringBuffer str = null;

		// compute the number of lines and the maximum width
		int nlines = 1, w = 0, start = 0, end = text.indexOf(m_delim);
		m_textDim.width = 0;
		String line;
		for (; end >= 0; ++nlines) {
			w = fm.stringWidth(line = text.substring(start, end));
			// abbreviate line as needed
			if (m_maxTextWidth > -1 && w > m_maxTextWidth) {
				if (str == null)
					str = new StringBuffer(text.substring(0, start));
				str.append(StringLib.abbreviate(line, fm, m_maxTextWidth));
				str.append(m_delim);
				w = m_maxTextWidth;
			} else if (str != null) {
				str.append(line).append(m_delim);
			}
			// update maximum width and substring indices
			m_textDim.width = Math.max(m_textDim.width, w);
			start = end + 1;
			end = text.indexOf(m_delim, start);
		}
		w = fm.stringWidth(line = text.substring(start));
		// abbreviate line as needed
		if (m_maxTextWidth > -1 && w > m_maxTextWidth) {
			if (str == null)
				str = new StringBuffer(text.substring(0, start));
			str.append(StringLib.abbreviate(line, fm, m_maxTextWidth));
			w = m_maxTextWidth;
		} else if (str != null) {
			str.append(line);
		}
		// update maximum width
		m_textDim.width = Math.max(m_textDim.width, w);

		// compute the text height
		m_textDim.height = fm.getHeight() * nlines;

		return str == null ? text : str.toString();
	}

}
