package cn.edu.njau.lufuse.vis;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;

import prefuse.Display;

public class MaxViser {
	private Display dis;
	private JFrame frame;

	public MaxViser(Display dis) {
		this.dis = dis;
	}

	public void show() {
		Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
		dis.addKeyListener(new KeyAdapter() { // 主窗口添加键盘监听器
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_S) {
					System.out.println("单击了S");
					frame.setVisible(false);
				}
			}
		});

		frame = new JFrame("---");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(screensize);
		frame.add(dis);
		frame.setUndecorated(true); // 去掉窗口的装饰
		frame.setVisible(true);
	}
}
