package cn.edu.njau.lufuse.vis;

import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import cn.edu.njau.lufuse.Lufuse;
import cn.edu.njau.lufuse.io.CsvReader;
import cn.edu.njau.lufuse.util.ColorSet;

import prefuse.Constants;
import prefuse.Display;
import prefuse.Visualization;
import prefuse.action.ActionList;
import prefuse.action.RepaintAction;
import prefuse.action.assignment.ColorAction;
import prefuse.action.assignment.DataColorAction;
import prefuse.action.assignment.FontAction;
import prefuse.action.layout.Layout;
import prefuse.action.layout.graph.ForceDirectedLayout;
import prefuse.activity.Activity;
import prefuse.controls.DragControl;
import prefuse.controls.NeighborHighlightControl;
import prefuse.controls.PanControl;
import prefuse.controls.WheelZoomControl;
import prefuse.controls.ZoomControl;
import prefuse.controls.ZoomToFitControl;
import prefuse.data.Graph;
import prefuse.data.Tuple;
import prefuse.data.event.TupleSetListener;
import prefuse.data.io.DataIOException;
import prefuse.data.io.GraphMLReader;
import prefuse.data.tuple.TupleSet;
import prefuse.render.DefaultRendererFactory;
import prefuse.render.EdgeRenderer;
import prefuse.render.LabelRenderer;
import prefuse.render.PolygonRenderer;
import prefuse.render.Renderer;
import prefuse.util.ColorLib;
import prefuse.util.FontLib;
import prefuse.util.GraphicsLib;
import prefuse.visual.AggregateItem;
import prefuse.visual.AggregateTable;
import prefuse.visual.EdgeItem;
import prefuse.visual.VisualGraph;
import prefuse.visual.VisualItem;

public class SnsCluShow extends Display {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String GRAPH = "graph";
	public static final String NODES = "graph.nodes";
	public static final String EDGES = "graph.edges";
	public static final String AGGR = "aggregates";

	// 构造函数
	public SnsCluShow(String xmlPath) throws IOException{
		super(new Visualization());
		initDataGroups(xmlPath);// 调用函数初始化数据组
		runVis();	
	}
	
	public SnsCluShow(Graph g) throws IOException{
		super(new Visualization());
		initDataGroups(g);// 调用函数初始化数据组
		runVis();	
	}
	
	private void runVis(){
		
		// 设置 Renderer
		LabelRenderer nodeR = new LabelRenderer(Lufuse.NODE_NAME);// 使用 node_id 来创建带有标签的节点
		nodeR.setRoundedCorner(Lufuse.NODE_CONER, Lufuse.NODE_CONER);
		nodeR.setMaxTextWidth(Lufuse.NODE_MAX_W);// 设置 Node 的最大长度
		
		EdgeRenderer edgeR = new EdgeRenderer(Constants.EDGE_TYPE_LINE,Constants.EDGE_ARROW_REVERSE);
		edgeR.setArrowHeadSize(5, 5);
		edgeR.setDefaultLineWidth(Lufuse.EDGE_BOUND);
		
		// 绘制弧形边缘的多边形聚集
		Renderer polyR = new PolygonRenderer(Constants.POLY_TYPE_CURVE);
		((PolygonRenderer) polyR).setCurveSlack(0.15f);// setCurveSlack:为弧形线条松弛设置参数

		DefaultRendererFactory drf = new DefaultRendererFactory();
		drf.setDefaultRenderer(nodeR);
		drf.setDefaultEdgeRenderer(edgeR);
		drf.add("ingroup('aggregates')", polyR);
		m_vis.setRendererFactory(drf);
		
		ColorAction nStroke = new ColorAction(NODES, VisualItem.STROKECOLOR);
		nStroke.setDefaultColor(ColorLib.gray(100));
		nStroke.add("_hover", ColorLib.gray(50));

		// 2012-6-18添加
		ColorAction fill = new ColorAction(NODES, VisualItem.FILLCOLOR,ColorLib.rgb(200, 200, 255));
		fill.add(VisualItem.FIXED, ColorLib.rgb(255, 100, 100));
		fill.add(VisualItem.HIGHLIGHT, ColorLib.rgb(255, 200, 125));
		
		// 选中聚集的动作
		TupleSet focusGroup = m_vis.getGroup(Visualization.FOCUS_ITEMS);
		focusGroup.addTupleSetListener(new TupleSetListener() {
			public void tupleSetChanged(TupleSet ts, Tuple[] add, Tuple[] rem) {
				for (int i = 0; i < rem.length; ++i)
					((VisualItem) rem[i]).setFixed(false);
				for (int i = 0; i < add.length; ++i) {
					((VisualItem) add[i]).setFixed(false);
					((VisualItem) add[i]).setFixed(true);
				}
				if (ts.getTupleCount() == 0) {
					ts.addTuple(rem[0]);
					((VisualItem) rem[0]).setFixed(false);
				}
				m_vis.run("draw");
			}
		});

		ColorAction nFill = new ColorAction(NODES, VisualItem.FILLCOLOR);
		nFill.setDefaultColor(ColorLib.gray(255));
		nFill.add("_hover", ColorLib.gray(200));

		ColorAction text = new ColorAction(NODES, VisualItem.TEXTCOLOR,ColorLib.gray(0));
		text.add("_hover", ColorLib.gray(255));

		ColorAction nEdges = new ColorAction(EDGES, VisualItem.STROKECOLOR);
		nEdges.setDefaultColor(ColorLib.gray(100));

		ColorAction aStroke = new ColorAction(AGGR, VisualItem.STROKECOLOR);
		aStroke.setDefaultColor(ColorLib.gray(200));
		aStroke.add("_hover", ColorLib.rgb(255, 100, 100));

		// 定义聚集背景颜色
		//int[] palette = new int[] { ColorLib.rgba(255, 200, 200, 150),ColorLib.rgba(200, 255, 200, 150),ColorLib.rgba(200, 200, 255, 150) };
		int[] palette = ColorSet.getBackLib(Lufuse.CLU_NUM);
		ColorAction aFill = new DataColorAction(AGGR, "id", Constants.NOMINAL,VisualItem.FILLCOLOR, palette);
		
		// 颜色
		ActionList colors = new ActionList();
		colors.add(text);
		colors.add(nStroke);
		colors.add(fill);
		colors.add(nEdges);
		colors.add(aStroke);
		colors.add(aFill);
		
		// 设置字体大小
		FontAction fonts = new FontAction(NODES, FontLib.getFont("Tahoma", Lufuse.NODE_FONT));
		//fonts.add("ingroup('_focus_')", FontLib.getFont("Tahoma", 16));

		// 现在创建主布局程序
		ActionList layout = new ActionList(Activity.INFINITY);
		layout.add(colors);
		layout.add(fonts);
		// layout.add(new ForceDirectedLayout(GRAPH, true));
		layout.add(new ColorAction(NODES, VisualItem.STROKECOLOR, 0));
		layout.add(new CluserShowLayout(AGGR));
		layout.add(new MyForceDirectedLayout(GRAPH,true));
		layout.add(new RepaintAction());		
		// 固定第一个节点
		//layout.add(new FixFirstNodeLayout());
				
		m_vis.putAction("layout", layout);

		// 设置显示效果
		pan(400, 400);
		setHighQuality(true);
		
		addControlListener(new DragControl());
		addControlListener(new ZoomControl());
		addControlListener(new ZoomToFitControl());// 右键单击使画面最适合状态 
		addControlListener(new PanControl());
		addControlListener(new WheelZoomControl()); // 滚轮缩放控制
		addControlListener(new NeighborHighlightControl()); // 邻节点亮度控制
		
		// run
		m_vis.run("layout");
		m_vis.runAfter("draw", "layout");
		
		//new Thread(new FixAllShow(this)).run();
	}
	

	// 初始化数据组
	private void initDataGroups(String xmlPath) throws IOException{
		Graph g = null;
		try {
			g = new GraphMLReader().readGraph(xmlPath);// 此步骤可能包括很多次的数据转换，这里读取一个XML文件
		} catch (DataIOException e) {
			e.printStackTrace();
			System.err.println("lujian863-initDataGroups");
			System.exit(1);
		}
		Lufuse.GRAPH_TMP = g;
		initDataGroups(g);
	}
	
	private void initDataGroups(Graph g) {
		
		// 添加一个可视化数据组 
		VisualGraph vg = m_vis.addGraph(GRAPH, g);
		m_vis.setInteractive(EDGES, null, false);
		m_vis.setValue(NODES, null, VisualItem.SHAPE, new Integer(Constants.SHAPE_ELLIPSE));

		AggregateTable at = m_vis.addAggregates(AGGR);
		at.addColumn(VisualItem.POLYGON, float[].class);
		at.addColumn("id", int.class);
		
		// 读取 data/clu 文件，获取聚类分组结果
		List<int[]> clu = CsvReader.getCsvCluster(Lufuse.CLU_CSV_PATH);
		int cluLen = clu.size();
		
		//System.out.println("聚类数量:"+cluLen);
		Lufuse.CLU_NUM = cluLen;
		
		for (int i = 0; i < cluLen; i++) {
			@SuppressWarnings("rawtypes")
			Iterator nodes = vg.nodes();
			int[] sTmp = clu.get(i);
			int[] tmp = new int[sTmp.length];
			for (int t = 0; t < sTmp.length; t++) {
				tmp[t] = sTmp[t];
			}
			AggregateItem aitem = (AggregateItem) at.addItem();
			aitem.setInt("id", i);
			int js = 0;
			for (int j = 0; j < tmp.length; j++) {
				for (int m = js; m < tmp[j]; m++) {
					nodes.next();
					js++;
				}
				aitem.addItem((VisualItem) nodes.next());
				js++;
			}
			js = 0;
		}

	}

	class MyForceDirectedLayout extends ForceDirectedLayout {
		float f;

		public MyForceDirectedLayout(String graph) {
			super(graph);
		}

		public MyForceDirectedLayout(String graph, boolean f) {
			super(graph,f);
		}
		
		public MyForceDirectedLayout(String graph, float f) {
			super(graph);
			this.f = f;
		}

		@Override
		protected float getSpringLength(EdgeItem e) {
			float f = (float) 0;
			// f = e.canGetFloat("length") ? e.getFloat("length") : 200;
			f = e.getFloat("length") * Lufuse.EDGE_RATE;
			return (float) f;
		}
	}
}

class CluserShowLayout extends Layout {

	private int m_margin = 5; // 边缘凸出的像素
	private double[] m_pts; // 计算凸出的缓冲区

	public CluserShowLayout(String aggrGroup) {
		super(aggrGroup);// 调用父类具有相同形参的构造函数
	}

	public void run(double frac) {

		AggregateTable aggr = (AggregateTable) m_vis.getGroup(m_group);
		
		int num = aggr.getTupleCount();
		if (num == 0)
			return;

		// 更新缓存
		int maxsz = 0;
		for (@SuppressWarnings("rawtypes")
		Iterator aggrs = aggr.tuples(); aggrs.hasNext();)
			maxsz = Math.max(maxsz,
					4 * 2 * ((AggregateItem) aggrs.next()).getAggregateSize());
		if (m_pts == null || maxsz > m_pts.length) {
			m_pts = new double[maxsz];
		}

		@SuppressWarnings("rawtypes")
		Iterator aggrs = m_vis.visibleItems(m_group);
		while (aggrs.hasNext()) {
			AggregateItem aitem = (AggregateItem) aggrs.next();

			int idx = 0;
			if (aitem.getAggregateSize() == 0)
				continue;
			VisualItem item = null;
			@SuppressWarnings("rawtypes")
			Iterator iter = aitem.items();
			while (iter.hasNext()) {
				item = (VisualItem) iter.next();
				if (item.isVisible()) {
					addPoint(m_pts, idx, item, m_margin);
					idx += 2 * 4;
				}
			}
			// 如果没有聚集，则do nothing~
			if (idx == 0)
				continue;
			
			double[] nhull = GraphicsLib.convexHull(m_pts, idx);

			float[] fhull = (float[]) aitem.get(VisualItem.POLYGON);
			if (fhull == null || fhull.length < nhull.length)
				fhull = new float[nhull.length];
			else if (fhull.length > nhull.length)
				fhull[nhull.length] = Float.NaN;

			for (int j = 0; j < nhull.length; j++)
				fhull[j] = (float) nhull[j];
			aitem.set(VisualItem.POLYGON, fhull);
			aitem.setValidated(false); // force invalidation
		}
	}

	private static void addPoint(double[] pts, int idx, VisualItem item,
			int growth) {
		Rectangle2D b = item.getBounds();
		double minX = (b.getMinX()) - growth, minY = (b.getMinY()) - growth;
		double maxX = (b.getMaxX()) + growth, maxY = (b.getMaxY()) + growth;
		pts[idx] = minX;
		pts[idx + 1] = minY;
		pts[idx + 2] = minX;
		pts[idx + 3] = maxY;
		pts[idx + 4] = maxX;
		pts[idx + 5] = minY;
		pts[idx + 6] = maxX;
		pts[idx + 7] = maxY;
	}

}
