package cn.edu.njau.lufuse;

import java.util.List;

import prefuse.data.Graph;
import cn.edu.njau.lufuse.data.NEPack;

// 静态参数配置类
public class Lufuse {

	public static String	PRO_NAME		= "Lufuse";		// 项目名称
	public static String	PRO_VERSION		= "0.1.3";		// 项目版本号
	public static String	DEV_VERSION		= "0.1.3.0115";	// 开发版本号
	public static String	PRO_AUTHOR		= "lujian863";	// 作者
	public static String	PRO_DATE		= "2012-01-13";	// 更新日期
	
	public static String	XLS_ENCODING	= "GBK";		// xls文件编码
	public static String	MAR_XLS_PATH	= "";			// xls文件路径
	
	public static int		CLU_NUM			= 6;			// 默认聚类个数
	public static int		CLU_MODE		= 1;			// 读取聚集数据的方式，0表示从文件读取，1表示从List<int[]>读取
	public static String	CLU_CSV_PATH	= "";
	public static List<int[]>	CLU_DATA 	= null;			// 聚类数据
	
	public static int		VIS_WIDTH		= 800;			// 可视化窗口宽度默认值
	public static int		VIS_HEIGHT		= 600;			// 可视化窗口高度默认值
	
	public static float		EDGE_LEN_MAX	= 400;			// Edge最大长度默认值
	public static float		EDGE_LEN_MIN	= 50;			// Edge最小长度默认值
	
	public static int		NODE_NUM		= 20;			// Node个数，读取xls文件后修改
	public static int		EDGE_NUM		= 20;			// Edge个数，读取xls文件后修改
	
	public static String	GRA_XML_PATH	= "";			// 默认XML路径
	
	public static NEPack	NEP				= null;			// 保存 Node 和 Edge
	public static Graph		GRAPH_TMP		= null;			// 重绘读取此 Graph

	
	// Vis设置
	public static int		NODE_CONER		= 8;			// Node 的圆角
	public static int		NODE_MAX_W		= 80;			// Node 的最大长度
	public static String 	NODE_NAME		= "node_id";	// Node 的名称
	public static int		NODE_FONT		= 10;			// Node 字体大小，关系到节点大小
	public static float		EDGE_RATE		= 1;			// Edge 倍率，多少倍
	public static int 		NODE_LENGTH		= 80;			// Node 最大长度
	public static double	EDGE_BOUND		= 1.0;			// Edge 粗细
	public static int		VIS_STATE		= 0;			// 0 表示静态，1 表示动态

}
