package cn.edu.njau.lufuse.algo;

import java.util.ArrayList;

import cn.edu.njau.lufuse.Lufuse;
import cn.edu.njau.lufuse.data.EdgeModel;
import cn.edu.njau.lufuse.data.NEPack;
import cn.edu.njau.lufuse.data.NodeModel;
import cn.edu.njau.lufuse.io.GraphXmlWriter;
import prefuse.data.Graph;
import prefuse.data.io.DataIOException;

// 矩阵转换类
public class MatrixChanger {
	
	private String[][] mat;
	private String[] nodeTags;
	private int nodeNum;
	private int edgeNum;
	
//	private NodeModel[] nms;
//	private EdgeModel[] ems;
	
	private ArrayList<NodeModel> nmArr = new ArrayList<NodeModel>();
	private ArrayList<EdgeModel> emArr = new ArrayList<EdgeModel>();
	
	public MatrixChanger(String[][] mat){
		this.mat = mat;
		this.preProcess();
	}
	
	// 数据预处理
	private void preProcess(){
		nodeNum = mat.length;
		nodeTags = new String[nodeNum];
		edgeNum = 0;
		ArrayList<Integer> links = new ArrayList<Integer>();
		for(int i = 0; i < nodeNum; i++){
			if(i != 0){
				nodeTags[i] = mat[i][0];
				nmArr.add(new NodeModel(mat[i][0], i-1));
			}	
			for(int j = 0; j < nodeNum; j++){
				if((i >0 && j >0) && (i < j && Integer.parseInt(mat[i][j]) != 0)){
					edgeNum++;
					links.add(Integer.parseInt(mat[i][j]));
					emArr.add(new EdgeModel(i-1,j-1,0));// length转换
				}
			}
		}
		
		// 设置 Edge 的阈值
		Lufuse.NODE_NUM = nodeNum;
		Lufuse.EDGE_NUM = edgeNum;
		EdgeLenThre elt = new EdgeLenThre(Lufuse.VIS_WIDTH,Lufuse.VIS_HEIGHT,nodeNum,edgeNum);
		Lufuse.EDGE_LEN_MAX = elt.getMaxLen();
		Lufuse.EDGE_LEN_MIN = elt.getMinLen();
		
		// 计算 Edge 的长度
		EdgeLenChanger elc = new EdgeLenChanger();
		float[] tlinks = elc.getTrueEdgeLength(links);
		for(int i = 0; i < edgeNum; i++){
			emArr.get(i).setLength(tlinks[i]);
		}
	}
	
	// 将矩阵转换成Prefuse显示用的Graph（并生成XML）
	public Graph matrixToGraph(){
		
		// 包装
		NodeModel[] nms = new NodeModel[nmArr.size()];
		nmArr.toArray(nms);
		EdgeModel[] ems = new EdgeModel[emArr.size()];
		emArr.toArray(ems);
		NEPack nep = new NEPack(nms,ems);
		
		Lufuse.NEP = nep;
		
		Graph gra = null;
		try {
			gra = GraphXmlWriter.transNepToXml(nep);
		} catch (DataIOException e) {
			e.printStackTrace();
		}
		//Lufuse.GRAPH_TMP = gra;
		return gra;
	}
	
}
