package cn.edu.njau.lufuse.algo;

import cn.edu.njau.lufuse.Lufuse;

public class WordsMatrix {
	
	int N;//总文档数
	int Nx;//含词x的文档数
	int Ny;//含词y的文档数
	int Nxy;//含词x和y的文档数
	
	public WordsMatrix(int N,int Nx,int Ny, int Nxy){
		this.N = N;
		this.Nx = Nx;
		this.Ny = Ny;
		this.Nxy = Nxy;
	}
	
	//计算两个词的关系度,算法来源参考 "data/reference/关键词与关键词之间的相关度计算.txt"
	public double getMIValue(){
		double tmp = Math.log10(N/Nx)*Math.log10(N/Ny)*Nxy/(Nx+Ny-Nxy);
		if(tmp < 0.0){
			tmp = 0.0;
		}
		if(tmp >= 1.5){
			tmp = 1.5;
		}
		return tmp;
	}
	
	//计算两个词的关系度，并转换成Prefuse的Edge长度
	public int getMILength(){
		
		double MaxLen = Lufuse.EDGE_LEN_MAX; //默认最大长度为800，定义成double类型的，方便后面计算
		double MinLen = Lufuse.EDGE_LEN_MIN; //默认最小长度为50
		
		double val = this.getMIValue();
		
		//根据val值 0.0~1.0分配 MinLen~MaxLen的Edge长度
		if(val == 0.0){
			return 0;
		}
	
		double TmpLen = MinLen + (1.5 - val)*MaxLen;
		//double TmpLen = (MaxLen - MinLen) * val + MinLen;
		return (int)TmpLen;
	}
}
