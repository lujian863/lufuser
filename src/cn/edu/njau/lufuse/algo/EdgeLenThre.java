package cn.edu.njau.lufuse.algo;

// Edge长度阈值计算
public class EdgeLenThre {

	private int x;// 可视化窗口宽度
	private int y;// 可视化窗口高度
	
	private int n;// 节点数量
	private int e;// 边的数量
	
	private int min;// Edge最小值
	private int max;// Edge最大值
	
	public EdgeLenThre(int x, int y, int n, int e){
		this.x = x;
		this.y = y;
		this.n = n;
		this.e = e;
		this.cValue();
	}
	
	private void cValue(){
		min = y / e;
		max = x / ((int)Math.sqrt(n) - 1);
		//max = x / (n / 2 - 1);
	}
	
	public int getMaxLen(){
		return max;
	}
	
	public int getMinLen(){
		return min;
	} 
	
}
