package cn.edu.njau.lufuse.test;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public class CTabExample extends Composite {
	

	 public CTabExample(Composite parent, int style) {
	  super(parent, style);
	  setLayout(new FillLayout(SWT.HORIZONTAL));
	  
	  CTabFolder tabFolder = new CTabFolder(this, SWT.BORDER);
	  tabFolder.setSimple(false);
	  //tabFolder.setSelectionBackground();

	  CTabItem tbtmTabitem_1 = new CTabItem(tabFolder, SWT.NONE);
	  tbtmTabitem_1.setText("tabItem1");

	  Composite composite = new Composite(tabFolder, SWT.NONE);
	  tbtmTabitem_1.setControl(composite);
	  composite.setLayout(new GridLayout(1, false));
	  Label lblChild = new Label(composite, SWT.NONE);
	  lblChild.setText("child");

	  CTabItem tbtmTabitem = new CTabItem(tabFolder, SWT.NONE);
	  tbtmTabitem.setText("tabItem2");

	  Composite composite_1 = new Composite(tabFolder, SWT.NONE);
	  tbtmTabitem.setControl(composite_1);

	 }
	}

