package cn.edu.njau.lufuse.test;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;

import cn.edu.njau.lufuse.util.MyToolKit;

public class MyDragTreeTest {

	protected Shell shell;
	protected Display display;
	private StyledText sss;
	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			MyDragTreeTest window = new MyDragTreeTest();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(557, 415);
		shell.setText("SWT Tree 拖拽控制例子");
		
		CTabFolder tabFolder = new CTabFolder(shell, SWT.BORDER);
		tabFolder.setBounds(10, 10, 202, 357);
		tabFolder.setSelectionBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));
		
		CTabItem tbtmNewItem = new CTabItem(tabFolder, SWT.NONE);
		tbtmNewItem.setText("New Item");
		
		final Tree tree = new Tree(tabFolder, SWT.BORDER);
		tbtmNewItem.setControl(tree);
		
		TreeItem t0 = new TreeItem(tree, SWT.NONE);
		t0.setText("\u7C7B1");
		
		TreeItem t1 = new TreeItem(tree, SWT.NONE);
		t1.setText("\u7C7B2");
		
		TreeItem t2 = new TreeItem(tree, SWT.NONE);
		t2.setText("\u7C7B3");
		
		TreeItem t3 = new TreeItem(tree, SWT.NONE);
		t3.setText("nodes");
		
		TreeItem t30 = new TreeItem(t3, SWT.NONE);
		t30.setText("node01");
		
		TreeItem t31 = new TreeItem(t3, SWT.NONE);
		t31.setText("node02");
		
		TreeItem t32 = new TreeItem(t3, SWT.NONE);
		t32.setText("node03");
		
		TreeItem t33 = new TreeItem(t3, SWT.NONE);
		t33.setText("node04");
		
		TreeItem t34 = new TreeItem(t3, SWT.NONE);
		t34.setText("node05");
		t3.setExpanded(true);
		
		sss = new StyledText(shell, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		sss.setBounds(218, 10, 313, 357);
		
		Transfer[] types = new Transfer[] { TextTransfer.getInstance() };
		int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_LINK;
		
		// 建立拖拽 源————树tree对象
		final DragSource source = new DragSource(tree, operations);
		source.setTransfer(types);
		final TreeItem[] dragSourceItem = new TreeItem[1];
		// 添加侦听器
		source.addDragListener(new DragSourceListener() {
			public void dragStart(DragSourceEvent event) {
				TreeItem[] selection = tree.getSelection(); // 获取所选tree的节点
				// 判断所选节点是否存在并且是否为末端节点
				if (selection.length > 0 && selection[0].getItemCount() == 0){
					event.doit = true; // 启动拖拽功能
					dragSourceItem[0] = selection[0]; // 保存拖拽节点信息
				} else {
					event.doit = false;
				}
			};
			// 
			public void dragSetData(DragSourceEvent event) {
				event.data = dragSourceItem[0].getText(); // 保存拖拽节点信息到事件数据中
			}
			// 
			public void dragFinished(DragSourceEvent event) {
				if (event.detail == DND.DROP_MOVE)
					dragSourceItem[0].dispose();
				dragSourceItem[0] = null;
			}
		});

		// 建立拖拽 目的————树tree对象
		DropTarget target = new DropTarget(tree, operations);
		target.setTransfer(types);
		target.addDropListener(new DropTargetAdapter() {
			// 处理拖拽到目的上空时的方法
			public void dragOver(DropTargetEvent event) {
				event.feedback = DND.FEEDBACK_EXPAND | DND.FEEDBACK_SCROLL; // 响应拖拽动作，FEEDBACK_EXPAND表示拖拽到可以展开的节点时展开相应节点。
				if (event.item != null) {
					TreeItem item = (TreeItem) event.item;
					// 处理拖拽动作响应，根据拖拽的位置不同产生不同的响应
					Point pt = display.map(null, tree, event.x, event.y); // 获取拖拽当前位置点
					Rectangle bounds = item.getBounds(); // 获取目的节点的边框

					// 根据拖拽当前位置点与目的节点的边框位置设置响应，如显示为FEEDBACK_INSERT_BEFORE、FEEDBACK_INSERT_AFTER
					if (pt.y < bounds.y + bounds.height / 3) {
						event.feedback |= DND.FEEDBACK_INSERT_BEFORE;
					} else if (pt.y > bounds.y + 2 * bounds.height / 3) {
						event.feedback |= DND.FEEDBACK_INSERT_AFTER;
					} else {
						event.feedback |= DND.FEEDBACK_SELECT;
					}
				}
			}

			// 处理放下时的方法
			public void drop(DropTargetEvent event) {
				if (event.data == null) {
					event.detail = DND.DROP_NONE;
					return;
				}
				String text = (String) event.data;
				// 如果放下的位置没有节点
				if (event.item == null) {
					// 则新建立一个节点，以根节点为父节点
					TreeItem item = new TreeItem(tree, SWT.NONE);
					item.setText(text);
					//则留在原来的位置
//					TreeItem item = (TreeItem) event.item;
//					TreeItem father = item.getParentItem();
//					TreeItem item2 = new TreeItem(father, SWT.NONE);
//					item2.setText(text);
					print("放下的位置没有节点,新建立一个节点，以根节点为父节点");
				}
				// 如果放下的位置有节点
				else {
					TreeItem item = (TreeItem) event.item;
					Point pt = display.map(null, tree, event.x, event.y);
					Rectangle bounds = item.getBounds();
					TreeItem parent = item.getParentItem();
					if (parent != null) {
						TreeItem[] items = parent.getItems();
						int index = 0;
						for (int i = 0; i < items.length; i++) {
							if (items[i] == item) {
								index = i;
								break;
							}
						}
						if (pt.y < bounds.y + bounds.height / 3) {
							// 以当前节点的父节点为父建立一个子节点，并且插入到当前节点前
							TreeItem newItem = new TreeItem(parent, SWT.NONE,
									index);
							newItem.setText(text);
						} else if (pt.y > bounds.y + 2 * bounds.height / 3) {
							// 以当前节点的父节点为父建立一个子节点，并且插入到当前节点前
							TreeItem newItem = new TreeItem(parent, SWT.NONE,
									index + 1);
							newItem.setText(text);
						} else {
							// 以当前为父节点建立一个子节点
							TreeItem newItem = new TreeItem(item, SWT.NONE);
							newItem.setText(text);
						}

					} else {
						TreeItem[] items = tree.getItems();
						int index = 0;
						for (int i = 0; i < items.length; i++) {
							if (items[i] == item) {
								index = i;
								break;
							}
						}
						if (pt.y < bounds.y + bounds.height / 3) {
							TreeItem newItem = new TreeItem(tree, SWT.NONE,
									index);
							newItem.setText(text);
						} else if (pt.y > bounds.y + 2 * bounds.height / 3) {
							TreeItem newItem = new TreeItem(tree, SWT.NONE,
									index + 1);
							newItem.setText(text);
						} else {
							TreeItem newItem = new TreeItem(item, SWT.NONE);
							newItem.setText(text);
						}
					}

				}
			}
		});
	}
	
	// 打印方法
	private void print(String s){
		// 获取时间
		String time = MyToolKit.getSystemTime();
		sss.append(time + " : " + s+"\n");
		sss.setSelection(sss.getCharCount());// 跳转到最后一行
	}
}
