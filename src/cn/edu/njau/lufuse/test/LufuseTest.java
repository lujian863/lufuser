package cn.edu.njau.lufuse.test;

import java.awt.Frame;
import java.io.IOException;

import javax.swing.JPanel;

import jxl.read.biff.BiffException;

import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.awt.SWT_AWT;

import cn.edu.njau.lufuse.Lufuse;
import cn.edu.njau.lufuse.algo.MatrixChanger;
import cn.edu.njau.lufuse.io.MatrixXlsReader;
import cn.edu.njau.lufuse.vis.SnsCluShow;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.custom.CTabItem;

import prefuse.data.Graph;

public class LufuseTest {

	public static String fpath = null;
	
	protected Shell shlLufuserv;
	private Composite composite;
	private Frame myframe;
	private JPanel panel;
	private StyledText sss;
	private Graph gra;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			LufuseTest window = new LufuseTest();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		org.eclipse.swt.widgets.Display display = org.eclipse.swt.widgets.Display.getDefault();
		createContents();
		shlLufuserv.open();
		shlLufuserv.layout();
		while (!shlLufuserv.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlLufuserv = new Shell(SWT.MIN);
		shlLufuserv.setMinimumSize(new Point(1000, 700));
		shlLufuserv.setSize(1022, 606);
		shlLufuserv.setText(Lufuse.PRO_NAME + "r-v" +Lufuse.PRO_VERSION + " by " + Lufuse.PRO_AUTHOR);
		
		//设置窗体居中显示
		int scrWid = shlLufuserv.getMonitor().getClientArea().width; 
		int scrHei = shlLufuserv.getMonitor().getClientArea().height;
		int x = shlLufuserv.getSize().x;
		int y = shlLufuserv.getSize().y;
		if(x > scrWid){
			shlLufuserv.getSize().x = scrWid;
		}
		if(y > scrHei){
			shlLufuserv.getSize().y = scrHei;
		}
		shlLufuserv.setLocation((scrWid - x)/2, (scrHei - y)/2);
		
		Menu menu = new Menu(shlLufuserv, SWT.BAR);
		shlLufuserv.setMenuBar(menu);
		
		MenuItem menuItem = new MenuItem(menu, SWT.NONE);
		menuItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 打开文件
				FileDialog fs=new FileDialog(shlLufuserv,SWT.SINGLE);
				fs.setFilterNames(new String[]{"*.xls"});
				fs.setFilterExtensions(new String[]{"*.xls"});
				String fp = fs.open();
				if(fp == null){
					System.out.println("未选择文件!");
					sss.append("未选择文件！\n");
				}else{
					fpath = fp;
					sss.append("已选择文件："+ fpath +"！\n");
				}
			}
		});
		menuItem.setText("\u6253\u5F00\u6570\u636E\u6E90");
		
		MenuItem menuItem_1 = new MenuItem(menu, SWT.NONE);
		menuItem_1.setText("\u5B57\u4F53+");
		
		MenuItem menuItem_2 = new MenuItem(menu, SWT.NONE);
		menuItem_2.setText("\u5B57\u4F53-");
		
		Group group = new Group(shlLufuserv, SWT.NONE);
		group.setBounds(791, 0, 205, 514);
		group.setText("\u63A7\u5236\u83DC\u5355");
		
		Button button = new Button(group, SWT.NONE);
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// 数据预处理
				Lufuse.VIS_WIDTH	= panel.getSize().width;
				Lufuse.VIS_HEIGHT	= panel.getSize().height;
				
				// 读取xls文件数据
				String[][] mat = null;
				try {
					mat = MatrixXlsReader.getMatrixData(fpath);
				} catch (BiffException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					sss.append("Error:12134");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					sss.append("Error:12234");
				}
				
				// 
				MatrixChanger mc = new MatrixChanger(mat);
				gra = mc.matrixToGraph();
				
				sss.append("数据预处理完成！");
			}
		});
		button.setBounds(10, 22, 91, 27);
		button.setText("\u6570\u636E\u9884\u5904\u7406");
		
		Button btnNewButton = new Button(group, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				SnsCluShow scs = null;
				try {
					if(fpath == null){
						scs = new SnsCluShow(Lufuse.GRA_XML_PATH);
					}else{
						scs = new SnsCluShow(gra);
					}
				} catch (IOException e1) {
					e1.printStackTrace();
					System.err.println("lujian863-init-scs");
				}
				
				scs.setSize(panel.getSize());
				panel.add(scs);
			}
		});
		btnNewButton.setBounds(107, 22, 91, 27);
		btnNewButton.setText("\u5F00\u59CB\u7ED8\u56FE");
		
		Button button_1 = new Button(group, SWT.NONE);
		button_1.setText("\u91CD\u65B0\u7ED8\u56FE");
		button_1.setBounds(10, 55, 91, 27);
		
		Button button_2 = new Button(group, SWT.NONE);
		button_2.setText("\u505C\u6B62\u7ED8\u56FE");
		button_2.setBounds(107, 55, 91, 27);
		
		CTabFolder tabFolder = new CTabFolder(group, SWT.BORDER);
		tabFolder.setBounds(10, 88, 188, 416);
		tabFolder.setSelectionBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));
		
		CTabItem tabItem = new CTabItem(tabFolder, SWT.NONE);
		tabItem.setText("\u805A\u7C7B\u83DC\u5355");
		
		CTabItem tabItem_1 = new CTabItem(tabFolder, SWT.NONE);
		tabItem_1.setText("\u989C\u8272\u83DC\u5355");
		
		Group group_1 = new Group(shlLufuserv, SWT.NONE);
		group_1.setText("\u53EF\u89C6\u5316");
		group_1.setBounds(10, 0, 775, 632);
		
		composite = new Composite(group_1, SWT.EMBEDDED);
		composite.setBounds(10, 20, 755, 602);
		
		myframe = SWT_AWT.new_Frame(composite);
		myframe.setSize(composite.getSize().x,composite.getSize().y);
		panel = new JPanel();
		panel.setSize(myframe.getSize());
		
		myframe.add(panel);
		
		Group group_2 = new Group(shlLufuserv, SWT.NONE);
		group_2.setText("\u7CFB\u7EDF\u72B6\u6001");
		group_2.setBounds(791, 520, 205, 112);
		
		sss = new StyledText(group_2, SWT.BORDER | SWT.READ_ONLY);
		sss.setTouchEnabled(true);
		sss.setBounds(10, 23, 185, 79);
	}
}
